package com.macro.mall.portal.service.impl;

import com.macro.mall.mapper.UmsTopicImageMapper;
import com.macro.mall.model.UmsTopicImage;
import com.macro.mall.model.UmsTopicImageExample;
import com.macro.mall.portal.dao.TopicImageDao;
import com.macro.mall.portal.service.UmsTopicImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsTopicImageServiceImpl implements UmsTopicImageService {

    @Autowired
    private UmsTopicImageMapper umsTopicImageMapper;

    @Autowired
    private TopicImageDao topicImageDao;

    @Override
    public List<UmsTopicImage> getTopicImageList(Integer topicId, Integer topicimageAuditstatus, Integer topicimageDelstatus) {
        UmsTopicImageExample umsTopicImageExample=new UmsTopicImageExample();
        umsTopicImageExample.createCriteria().andTopicidEqualTo(topicId)
                .andTopicimageauditstatusEqualTo(topicimageAuditstatus)
                .andTopicimagedelstatusEqualTo(topicimageDelstatus);
        return umsTopicImageMapper.selectByExample(umsTopicImageExample);
    }

    @Override
    public Integer batchInsertTopicimage(List<UmsTopicImage> umsTopicImages) {
        return topicImageDao.batchInsertTopicimage(umsTopicImages);
    }

    @Override
    public Integer deleteTopicimage(Integer topicId) {
        UmsTopicImageExample umsTopicImageExample=new UmsTopicImageExample();
        umsTopicImageExample.createCriteria().andTopicidEqualTo(topicId);
        return umsTopicImageMapper.deleteByExample(umsTopicImageExample);
    }
}
