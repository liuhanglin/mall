package com.macro.mall.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsCommentMapper;
import com.macro.mall.model.UmsComment;
import com.macro.mall.model.UmsCommentExample;
import com.macro.mall.portal.service.UmsCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsCommentServiceImpl implements UmsCommentService {

    @Autowired
    private UmsCommentMapper umsCommentMapper;


    @Override
    public List<UmsComment> getTopicCommentList(Integer topicId, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        UmsCommentExample umsCommentExample=new UmsCommentExample();
        UmsCommentExample.Criteria criteria= umsCommentExample.createCriteria();
        criteria.andTopicidEqualTo(topicId)
                .andCommentauditstatusEqualTo(1)
                .andCommentdelstatusEqualTo(0);
        umsCommentExample.setOrderByClause(" commentCreatetime ASC ");
        List<UmsComment> commentList=umsCommentMapper.selectByExample(umsCommentExample);
        return commentList;
    }

    @Override
    public Integer addTopicComment(UmsComment umsComment) {
        return umsCommentMapper.insertSelective(umsComment);
    }

    @Override
    public List<UmsComment> getParentCommentList(List<Integer> commentIds) {
        UmsCommentExample umsCommentExample=new UmsCommentExample();
        UmsCommentExample.Criteria criteria= umsCommentExample.createCriteria();
        criteria.andCommentidIn(commentIds)
                .andCommentauditstatusEqualTo(1)
                .andCommentdelstatusEqualTo(0);;
        return umsCommentMapper.selectByExample(umsCommentExample);
    }

    @Override
    public Long getTopicCommentCount(Integer topicId) {
        UmsCommentExample umsCommentExample=new UmsCommentExample();
        UmsCommentExample.Criteria criteria= umsCommentExample.createCriteria();
        criteria.andTopicidEqualTo(topicId)
                .andCommentauditstatusEqualTo(1)
                .andCommentdelstatusEqualTo(0);;
        return umsCommentMapper.countByExample(umsCommentExample);
    }
}
