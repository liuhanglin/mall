package com.macro.mall.portal.service;

import com.macro.mall.model.UmsTopicImage;

import java.util.List;

public interface UmsTopicImageService {
    List<UmsTopicImage> getTopicImageList(Integer topicId,Integer topicimageAuditstatus,Integer topicimageDelstatus);
    Integer batchInsertTopicimage(List<UmsTopicImage> umsTopicImages);
    Integer deleteTopicimage(Integer topicId);
}
