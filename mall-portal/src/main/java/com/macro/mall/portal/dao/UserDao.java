package com.macro.mall.portal.dao;

import com.macro.mall.model.UmsUser;
import com.macro.mall.portal.domain.FreindsResult;

import java.util.List;

public interface UserDao {
    List<FreindsResult> getUserfriends(String userId);
    UmsUser getUserBymemberid(String memberId);
}
