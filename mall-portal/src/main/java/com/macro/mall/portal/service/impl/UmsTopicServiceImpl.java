package com.macro.mall.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsTopicImageMapper;
import com.macro.mall.mapper.UmsTopicMapper;
import com.macro.mall.model.UmsTopic;
import com.macro.mall.model.UmsTopicExample;
import com.macro.mall.portal.dao.TopicDao;
import com.macro.mall.portal.dto.TopicDetailDto;
import com.macro.mall.portal.dto.TopicIndexDto;
import com.macro.mall.portal.service.UmsTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsTopicServiceImpl implements UmsTopicService {

    @Autowired
    private TopicDao topicDao;

    @Autowired
    private UmsTopicMapper umsTopicMapper;

    @Override
    public List<TopicIndexDto> getIndexShowTopicList(Integer pageIndex,Integer pageSize){
        PageHelper.startPage(pageIndex, pageSize);
        return topicDao.getIndexShowTopicList();
    }

    @Override
    public TopicDetailDto getTopicDetail(Integer topicId, Integer topicDelstatus, Integer topicAuditstatus, Integer topicPublishstatus) {
        return topicDao.getTopicDetail(topicId,topicDelstatus,topicAuditstatus,topicPublishstatus);
    }

    @Override
    public Integer insertSelective(UmsTopic umsTopic) {
        return umsTopicMapper.insertSelective(umsTopic);
    }

    @Override
    public Integer updateSelective(UmsTopic umsTopic) {
        return umsTopicMapper.updateByPrimaryKeySelective(umsTopic);
    }

    @Override
    public Integer update(UmsTopic umsTopic) {
        return  umsTopicMapper.updateByPrimaryKeySelective(umsTopic);

    }

    @Override
    public Integer updateBrowsecount(Integer topicId,Integer count) {
        return topicDao.updateBrowsecount(topicId,count);
    }

    @Override
    public Integer updatePraisecount(Integer topicId, Integer count) {
        return topicDao.updatePraisecount(topicId,count);
    }

    @Override
    public String getTopicUserid(Integer topicId) {
        return topicDao.getTopicUserid(topicId);
    }

}
