package com.macro.mall.portal.common;

public enum GenderEnum {
    Lady(0,"征女友"),
    MEN(1,"征男友");

    private final Integer key;

    private final String value;

    private GenderEnum(final Integer key,final String value){
        this.key = key;
        this.value = value;
    }

    public static Integer getkey(String value) {
        GenderEnum[] businessModeEnums = values();
        for (GenderEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.value().equals(value)) {
                return businessModeEnum.key();
            }
        }
        return null;
    }

    public static String getValue(Integer key) {
        GenderEnum[] businessModeEnums = values();
        for (GenderEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.key().equals(key)) {
                return businessModeEnum.value();
            }
        }
        return null;
    }


    public Integer key(){
        return this.key;
    }

    public String value(){
        return this.value;
    }
}
