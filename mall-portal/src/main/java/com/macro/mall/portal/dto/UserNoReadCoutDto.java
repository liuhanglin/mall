package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserNoReadCoutDto {
    private String fromuserId;
    private Integer noreadCount;
}
