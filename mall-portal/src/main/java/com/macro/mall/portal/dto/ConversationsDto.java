package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConversationsDto {
    private String content;
    private Integer conversationId;
    private String friendHeadUrl;
    private String friendId;
    private String friendName;
    private String msgUserId;
    private String timeStr;
    private Long timestamp;
    private String type;
    private Integer unread;
}
