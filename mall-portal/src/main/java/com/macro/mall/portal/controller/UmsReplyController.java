package com.macro.mall.portal.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.json.JSONUtil;
import com.macro.mall.common.api.CommonPage;
import com.macro.mall.common.api.CommonResult;
import com.macro.mall.model.UmsComment;
import com.macro.mall.model.UmsReply;
import com.macro.mall.portal.dto.CommentDto;
import com.macro.mall.portal.dto.ReplyDto;
import com.macro.mall.portal.dto.UserDetailDto;
import com.macro.mall.portal.service.UmsMemberService;
import com.macro.mall.portal.service.UmsReplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Api(tags = "UmsReplyController", description = "回复")
@RequestMapping("/reply")
public class UmsReplyController {

    @Autowired
    private UmsReplyService umsReplyService;
    @Autowired
    private UmsMemberService umsMemberService;

    @ApiOperation("获取评论回复列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getReplyList(@RequestParam(value = "topicId") Integer topicId,
                                     @RequestParam(value = "commentId") Integer commentId,
                                     @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                     @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex) {

        List<UmsReply> umsReplyList= umsReplyService.getCommentReplyList(commentId,pageIndex,pageSize);
        CommonPage<UmsReply> commonPage=CommonPage.restPage(umsReplyList);
        List<String> userIds=new ArrayList<String>();
        List<String> sendUserIds=umsReplyList.stream().map(UmsReply::getUserid).distinct().collect(Collectors.toList());
        List<String> parentUserIds=umsReplyList.stream().map(UmsReply::getParentuserid).distinct().collect(Collectors.toList());
        userIds=(List<String>)CollUtil.union(sendUserIds,parentUserIds).stream().distinct().collect(Collectors.toList());
        List<UserDetailDto> userDetailDtoList=umsMemberService.getUserDetail(userIds);

        List<ReplyDto> replyDtoList=new ArrayList<ReplyDto>();
        for (UmsReply umsReply:umsReplyList){
            ReplyDto replyDto=new ReplyDto();
            UserDetailDto userDetailDto=userDetailDtoList.stream().filter(s->s.getUserId().equals(umsReply.getUserid())).findAny().orElse(null);
            UserDetailDto puserDetailDto=userDetailDtoList.stream().filter(s->s.getUserId().equals(umsReply.getUserid())).findAny().orElse(null);
            replyDto.setCommentId(topicId);
            replyDto.setUserId(umsReply.getUserid());
            replyDto.setNickName(userDetailDto.getNickName());
            replyDto.setAvatorUrl(userDetailDto.getAvatorUrl());
            replyDto.setParentReplyid(umsReply.getReplyid());
            replyDto.setParentNickname(puserDetailDto.getNickName());
            replyDto.setParentAvatorurl(puserDetailDto.getAvatorUrl());
            replyDto.setParentUserid(umsReply.getParentuserid());
            replyDto.setReplyContent(umsReply.getReplycontent());
            replyDto.setReplyCreatetime(umsReply.getReplycreatetime());
            replyDto.setReplyPraisecount(0);
            replyDtoList.add(replyDto);
        }

        CommonPage<ReplyDto> result=new CommonPage<ReplyDto>();
        result.setList(replyDtoList);
        result.setPageNum(commonPage.getPageNum());
        result.setPageSize(commonPage.getPageSize());
        result.setTotal(commonPage.getTotal());
        result.setTotalPage(commonPage.getTotalPage());
        return CommonResult.success(result);
    }

    @ApiOperation("添加回复")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult addReply(@RequestParam("userId") String userId,
                                @RequestParam("topicId") Integer topicId,
                                @RequestParam("commentId") Integer commentId,
                                @RequestParam("parentReplyid") Integer parentReplyid,
                                @RequestParam("parentUserid") String parentUserid,
                                @RequestParam("replyContent") String replyContent) {
        UmsReply umsReply=new UmsReply();
        umsReply.setReplycreatetime(DateTime.now().toString());
        umsReply.setTopicid(topicId);
        umsReply.setCommentid(commentId);
        umsReply.setParentreplyid(parentReplyid);
        umsReply.setParentuserid(parentUserid);
        umsReply.setUserid(userId);
        umsReply.setReplycontent(replyContent);
        umsReply.setReplyauditstatus(1);
        umsReply.setReplydelstatus(0);
        umsReply.setReplypraisecount(0);
        Integer replyId= umsReplyService.addReply(umsReply);
        umsReply.setReplyid(replyId);
        return CommonResult.success(umsReply);
    }
}