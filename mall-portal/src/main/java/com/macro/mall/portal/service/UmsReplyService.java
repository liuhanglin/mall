package com.macro.mall.portal.service;

import com.macro.mall.model.UmsReply;
import com.macro.mall.portal.dto.CommentDto;

import java.util.List;

public interface UmsReplyService {
    List<CommentDto> getTopicCommentReplyTotalCount(Integer topicId, List<Integer> commentId);

    List<UmsReply> getCommentReplyList(Integer commentId, Integer pageIndex, Integer pageSize);

    Integer addReply(UmsReply umsReply);
}
