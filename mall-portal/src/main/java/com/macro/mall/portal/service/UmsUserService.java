package com.macro.mall.portal.service;

import com.macro.mall.model.UmsUser;

public interface UmsUserService {
    Integer addUser(UmsUser umsUser);
    UmsUser findUserByopenid(String openId);
}
