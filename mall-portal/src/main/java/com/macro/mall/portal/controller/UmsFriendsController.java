package com.macro.mall.portal.controller;

import com.macro.mall.common.api.CommonPage;
import com.macro.mall.common.api.CommonResult;
import com.macro.mall.model.UmsFriends;
import com.macro.mall.model.UmsMember;
import com.macro.mall.portal.domain.FreindsResult;
import com.macro.mall.portal.service.UmsFriendsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.List;


@Controller
@Api(tags = "UmsFriendsController", description = "会员好友管理")
@RequestMapping("/friends")
public class UmsFriendsController {

    @Autowired
    private UmsFriendsService umsFriendsService;

    @ApiOperation("获取会员好友列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getFriends(@RequestParam(value = "userId", required = false) String userId,
                                   @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                   @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex) {
        if(userId==null){
            return CommonResult.unauthorized(null);
        }
        List<FreindsResult> umsFriendsList =umsFriendsService.getUserFriends(userId,pageIndex,pageSize) ;
        return CommonResult.success(CommonPage.restPage(umsFriendsList));
    }


    @ApiOperation("会员添加好友")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult addFriends(@RequestParam(value = "userId", required = false) String userId,
                                   @RequestParam(value = "fuserId", required = false) String fuserId
                                   ) throws Exception {
        if(userId==null){
            return CommonResult.validateFailed("用户编号不能为空");
        }
        Boolean res= umsFriendsService.addFriend(userId,fuserId);
        return CommonResult.success(res);
    }

}
