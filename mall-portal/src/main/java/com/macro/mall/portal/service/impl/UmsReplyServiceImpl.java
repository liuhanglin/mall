package com.macro.mall.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsReplyMapper;
import com.macro.mall.model.UmsReply;
import com.macro.mall.model.UmsReplyExample;
import com.macro.mall.portal.dao.ReplyDao;
import com.macro.mall.portal.dto.CommentDto;
import com.macro.mall.portal.dto.ReplyDto;
import com.macro.mall.portal.service.UmsReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsReplyServiceImpl implements UmsReplyService {

    @Autowired
    private ReplyDao replyDao;

    @Autowired
    private UmsReplyMapper umsReplyMapper;

    @Override
    public List<CommentDto> getTopicCommentReplyTotalCount(Integer topicId, List<Integer> commentIds) {

        return replyDao.getTopicCommentReplyTotalCount(topicId,commentIds);
    }

    @Override
    public List<UmsReply> getCommentReplyList(Integer commentId, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        UmsReplyExample umsReplyExample=new UmsReplyExample();
        umsReplyExample.createCriteria().andCommentidEqualTo(commentId);
        umsReplyExample.setOrderByClause(" replyCreatetime ASC ");
        return umsReplyMapper.selectByExample(umsReplyExample);
    }

    @Override
    public Integer addReply(UmsReply umsReply) {
        return umsReplyMapper.insertSelective(umsReply);
    }
}
