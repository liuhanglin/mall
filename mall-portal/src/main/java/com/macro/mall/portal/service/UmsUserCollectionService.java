package com.macro.mall.portal.service;

import com.macro.mall.model.UmsUserAttention;
import com.macro.mall.model.UmsUserCollect;

import java.util.List;

public interface UmsUserCollectionService {
    Integer add(UmsUserCollect userCollect);
    Integer cancel(String userId,Integer topicId);
    List<UmsUserCollect> list(String userId,Integer pageIndex,Integer pageSize);
}
