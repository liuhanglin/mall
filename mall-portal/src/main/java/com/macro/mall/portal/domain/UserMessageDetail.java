package com.macro.mall.portal.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMessageDetail {
    private String fromuserId;
    private String nickName;
    private String noreadCount;
    private String lastmessageObj;
}
