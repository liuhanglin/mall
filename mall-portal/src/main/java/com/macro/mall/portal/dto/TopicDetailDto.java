package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TopicDetailDto {
    private Integer topicId;
    private String userId;
    private Integer topicAge;
    private String nickName;
    private String avatorUrl;
    private String topicTitle;
    private String topicContent;
    private String topicCreatetime;
    private String topicCover;
    private List<String> topicImages;
    private Integer topicPublishstatus;
    private Integer topicAuditstatus;
    private Integer topicDelstatus;
    private Integer topicTopstatus;
    private Integer topicGendertype;
    private String topicGendertypestr;
    private Integer topicHeight;
    private Integer topicWeight;
    private Integer topicEducation;
    private String topicEducationstr;
    private Integer topicFacescore;
    private String topicFacescorestr;
    private Integer topicMarried;
    private String topicMarriedstr;
    private String topicSelfintroduction;
    private String topicDemand;
    private String topicInterest;
    private String topicTags;
    private String topicAreacode;
    private String topicModifytime;
    private Integer topicBrowsecount;
    private Integer topicCommentcount;
    private Integer topicPraisecount;
    private Integer topicTemplatestatus;
}
