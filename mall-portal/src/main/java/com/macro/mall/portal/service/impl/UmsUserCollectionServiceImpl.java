package com.macro.mall.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsUserAttentionMapper;
import com.macro.mall.mapper.UmsUserCollectMapper;
import com.macro.mall.model.UmsUserCollect;
import com.macro.mall.model.UmsUserCollectExample;
import com.macro.mall.portal.service.UmsUserCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsUserCollectionServiceImpl implements UmsUserCollectionService {

    @Autowired
    private UmsUserCollectMapper userCollectMapper;

    @Override
    public Integer add(UmsUserCollect userCollect) {
        return userCollectMapper.insert(userCollect);
    }

    @Override
    public Integer cancel(String userId, Integer topicId) {
        UmsUserCollectExample userCollectExample=new UmsUserCollectExample();
        userCollectExample.createCriteria().andTopicidEqualTo(topicId).andUseridEqualTo(userId);
        return userCollectMapper.deleteByExample(userCollectExample);
    }

    @Override
    public List<UmsUserCollect> list(String userId,Integer pageIndex,Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        UmsUserCollectExample userCollectExample=new UmsUserCollectExample();
        userCollectExample.createCriteria().andUseridEqualTo(userId).andUsercollectdelstatusEqualTo(0);
        userCollectExample.setOrderByClause(" usercollectCreatetime DESC");
        return userCollectMapper.selectByExample(userCollectExample);
    }
}