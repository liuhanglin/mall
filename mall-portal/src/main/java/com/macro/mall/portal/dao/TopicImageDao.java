package com.macro.mall.portal.dao;

import com.macro.mall.model.UmsTopic;
import com.macro.mall.model.UmsTopicImage;

import java.util.List;

public interface TopicImageDao {
    Integer batchInsertTopicimage(List<UmsTopicImage> umsTopicList);
}
