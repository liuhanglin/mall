package com.macro.mall.portal.dto;

import com.macro.mall.model.UmsMessage;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;

@Getter
@Setter
public class MessageDto {
    private String content;
    private String friendId;
    private String headUrl;
    private Boolean isMy;
    private Boolean isPlaying;
    private Integer msgId;
    private String sendStatus;
    private Boolean showTime;
    private String time;
    private Long timestamp;
    private String type;
    private Integer voiceDuration;
}
