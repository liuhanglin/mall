package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopicIndexDto {
    private int topicId;
    private String topicTitle;
    private String topicContent;
    private String topicAge;
    private String topicAreacode;
    private Integer topicHeight;
    private String topicCover;
    private Integer topicGendertype;
    private String userId;
    private String topicCreatetime;
    private String topicModifytime;
}
