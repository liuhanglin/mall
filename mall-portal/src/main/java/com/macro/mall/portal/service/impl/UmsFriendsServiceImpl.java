package com.macro.mall.portal.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.lang.UUID;
import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsFriendsMapper;
import com.macro.mall.model.UmsFriends;
import com.macro.mall.model.UmsFriendsExample;
import com.macro.mall.portal.dao.UserDao;
import com.macro.mall.portal.domain.FreindsResult;
import com.macro.mall.portal.service.UmsFriendsService;
import com.macro.mall.security.lock.CacheLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsFriendsServiceImpl implements UmsFriendsService {


    @Autowired
    private UmsFriendsMapper umsFriendsMapper;

    @Autowired
    private UserDao userDao;

    @Override
    public List<UmsFriends> getFriends(String userId) {
        UmsFriendsExample umsFriendsExample=new UmsFriendsExample();
        umsFriendsExample.createCriteria().andUseridEqualTo(userId);
        List<UmsFriends> umsFriendsList= umsFriendsMapper.selectByExample(umsFriendsExample);
        return umsFriendsList;
    }

    @Override
    public List<FreindsResult> getUserFriends(String userId,Integer pageIndex,Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        List<FreindsResult> umsFriendsList= userDao.getUserfriends(userId);
        return umsFriendsList;
    }

    //@CacheLock(key = "friends",expire = 10000,prefix = "yanyuan")
    @Override
    public Boolean addFriend(String userId, String fuserId) throws  Exception {
        UmsFriends umsFriends=new UmsFriends();
        umsFriends.setFriendid(UUID.randomUUID().toString());
        umsFriends.setFuserid(fuserId);
        umsFriends.setUserid(userId);
        umsFriends.setStatus(0);
        umsFriends.setCreatetime(DateTime.now().toString());
        Integer res= umsFriendsMapper.insert(umsFriends);
        return res>0;
    }

    @Override
    public Boolean isFriend(String userId, String fuserId) {
        UmsFriendsExample umsFriendsExample=new UmsFriendsExample();
        umsFriendsExample.createCriteria()
                .andUseridEqualTo(userId)
                .andFuseridEqualTo(fuserId);
        long count= umsFriendsMapper.countByExample(umsFriendsExample);
        return count>0;
    }
}
