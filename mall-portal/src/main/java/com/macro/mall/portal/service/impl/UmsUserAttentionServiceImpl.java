package com.macro.mall.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsUserAttentionMapper;
import com.macro.mall.model.UmsUserAttention;
import com.macro.mall.model.UmsUserAttentionExample;
import com.macro.mall.portal.service.UmsUserAttentionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsUserAttentionServiceImpl implements UmsUserAttentionService {

    @Autowired
    private UmsUserAttentionMapper userAttentionMapper;

    @Override
    public Integer add(UmsUserAttention userAttention) {
        return userAttentionMapper.insert(userAttention);
    }

    @Override
    public Integer cancel(String userId, String fuserId) {
        UmsUserAttentionExample userAttentionExample=new UmsUserAttentionExample();
        userAttentionExample.createCriteria().andFuseridEqualTo(userId).andFuseridEqualTo(fuserId);
        return userAttentionMapper.deleteByExample(userAttentionExample);
    }

    @Override
    public List<UmsUserAttention> list(String userId,Integer pageIndex,Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        UmsUserAttentionExample userAttentionExample=new UmsUserAttentionExample();
        userAttentionExample.createCriteria().andFuseridEqualTo(userId).andUserattentiondelstatusEqualTo(0);
        userAttentionExample.setOrderByClause(" userattentionCreatetime DESC");
        return userAttentionMapper.selectByExample(userAttentionExample);
    }
}
