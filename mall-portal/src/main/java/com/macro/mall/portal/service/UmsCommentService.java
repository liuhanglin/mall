package com.macro.mall.portal.service;

import com.macro.mall.model.UmsComment;

import java.util.List;

public interface UmsCommentService {

    List<UmsComment> getTopicCommentList(Integer topicId,Integer pageIndex,Integer pageSize);

    Integer addTopicComment(UmsComment umsComment);

    List<UmsComment> getParentCommentList(List<Integer> commentIds);

    Long getTopicCommentCount(Integer topicId);
}
