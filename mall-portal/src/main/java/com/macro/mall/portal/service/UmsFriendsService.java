package com.macro.mall.portal.service;

import com.macro.mall.model.UmsFriends;
import com.macro.mall.portal.domain.FreindsResult;

import java.util.List;

public interface UmsFriendsService {
    List<UmsFriends> getFriends(String userId);

    Boolean addFriend(String userId,String fuserId) throws  Exception;

    Boolean isFriend(String userId,String fuserId);

    List<FreindsResult> getUserFriends(String userId, Integer pageIndex, Integer pageSize);
}
