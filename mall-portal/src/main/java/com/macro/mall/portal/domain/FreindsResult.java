package com.macro.mall.portal.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FreindsResult {
    private String userId;
    private String nickname;
    private String icon;
    private Integer gender;
}
