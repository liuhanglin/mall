package com.macro.mall.portal.dao;

import com.macro.mall.portal.dto.CommentDto;

import java.util.List;

public interface CommentDao {
    List<CommentDto> getTopicCommentList(Integer topicId,Integer pageIndex,Integer pageSize);
}
