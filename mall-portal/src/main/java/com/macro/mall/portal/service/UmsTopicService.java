package com.macro.mall.portal.service;

import com.macro.mall.model.UmsTopic;
import com.macro.mall.portal.dto.TopicDetailDto;
import com.macro.mall.portal.dto.TopicIndexDto;

import java.util.List;

public interface UmsTopicService {
    List<TopicIndexDto> getIndexShowTopicList(Integer pageIndex,Integer pageSize);
    TopicDetailDto getTopicDetail(Integer topicId, Integer topicDelstatus, Integer topicAuditstatus, Integer topicPublishstatus);
    Integer insertSelective(UmsTopic umsTopic);
    Integer updateSelective(UmsTopic umsTopic);
    Integer update(UmsTopic umsTopic);
    Integer updateBrowsecount(Integer topicId,Integer count);
    Integer updatePraisecount(Integer topicId,Integer count);
    String getTopicUserid(Integer topicId);
}
