package com.macro.mall.portal.websocket;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.macro.mall.model.UmsFriends;
import com.macro.mall.model.UmsMessage;
import com.macro.mall.portal.common.MallEnum;
import com.macro.mall.portal.component.SpringUtils;
import com.macro.mall.portal.dto.MessageDto;
import com.macro.mall.portal.service.UmsFriendsService;
import com.macro.mall.portal.service.UmsMessageService;
import com.macro.mall.portal.util.DateUtil;
import com.macro.mall.security.util.SpringUtil;
import com.mongodb.internal.connection.ConcurrentPool;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint("/imserver/{userId}")
@Component
public class    WebSocketServer {

    static Log log= LogFactory.get(WebSocketServer.class);
    /**静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。*/
    private static int onlineCount = 0;
    /**concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。*/
    private static ConcurrentHashMap<String,WebSocketServer> webSocketMap = new ConcurrentHashMap<>();
    /**与某个客户端的连接会话，需要通过它来给客户端发送数据*/
    private Session session;
    /**接收userId*/
    private String userId="";

    private UmsFriendsService umsFriendsService;

    private UmsMessageService umsMessageService;


    public WebSocketServer(){

    }

    //此处是解决无法注入的关键
    private static ApplicationContext applicationContext;

    public static void setApplicationContext(ApplicationContext applicationContext) {
        WebSocketServer.applicationContext = applicationContext;
    }


    /**
     * 连接建立成功调用的方法*/
    @OnOpen
    public void onOpen(Session session,@PathParam("userId") String userId) throws  Exception{
        this.session = session;
        this.userId=userId;
        if(webSocketMap.containsKey(userId)){
            webSocketMap.remove(userId);
            webSocketMap.put(userId,this);
            //加入set中
        }else{
            webSocketMap.put(userId,this);
            //加入set中
            addOnlineCount();
            //在线数加1
        }

        log.info("用户连接:"+userId+",当前在线人数为:" + getOnlineCount());

        try {
            //查询离线消息
            umsMessageService = applicationContext.getBean(UmsMessageService.class);
            List<UmsMessage> umsMessageList= umsMessageService.getNoSendMessage(userId);
            List<Integer> messageIds=new ArrayList<Integer>();
            for (UmsMessage umsMessage : umsMessageList){
                try {
                    Long timeStamp=DateUtil.dateToStamp(umsMessage.getCreatetime());

                    Date createTime= cn.hutool.core.date.DateUtil.parseDateTime(umsMessage.getCreatetime());
                    Long differHoure = cn.hutool.core.date.DateUtil.between(new Date(),createTime, DateUnit.MINUTE);
                    SimpleDateFormat formatter=new SimpleDateFormat(differHoure>24?"yyyy-MM-dd HH:mm:ss":"HH:mm");
                    String dateString=formatter.format(createTime);
                    Map msg=new HashMap();
                    msg.put("content",umsMessage.getMessagetext());
                    msg.put("friendId",umsMessage.getFromuserid());
                    msg.put("msgUserId",userId);
                    msg.put("timeStr",dateString);
                    msg.put("timestamp",timeStamp);
                    msg.put("type",MallEnum.getValue(umsMessage.getMessagetype()));
                    msg.put("userId",userId);
                    msg.put("duration",umsMessage.getDuration());
                    sendMessage(JSONObject.toJSONString(msg));
                    umsMessage.setSendstatus(1);
                    messageIds.add(umsMessage.getMessageid());
                } catch (IOException e) {
                    log.info("用户端连接异常:"+e.getMessage());
                }

            }
            if(messageIds.size()>0) {
                umsMessageService.updateMessageSendStatus(messageIds);
            }
        } catch (Exception e) {
            log.error("服务端异常："+e.getMessage());
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        if(webSocketMap.containsKey(userId)){
            webSocketMap.remove(userId);
            //从set中删除
            subOnlineCount();
        }
        log.info("用户退出:"+userId+",当前在线人数为:" + getOnlineCount());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) throws Exception {
        log.info("用户消息:"+userId+",报文:"+message);
        //可以群发消息
        //消息保存到数据库、redis
        if(StringUtils.isNotBlank(message)){
            try {
                JSONObject jsonObject = JSON.parseObject(message);
                Long timeStamp= cn.hutool.core.date.DateUtil.current(false);
                SimpleDateFormat formatter=new SimpleDateFormat("HH:mm");
                Map msg=new HashMap();
                msg.put("content",jsonObject.get("content").toString());
                msg.put("friendId",jsonObject.get("friendId").toString());
                msg.put("msgUserId",jsonObject.get("userId").toString());
                msg.put("timeStr",formatter.format(new Date()));
                msg.put("timestamp",timeStamp);
                msg.put("type",jsonObject.get("type").toString());
                msg.put("userId",jsonObject.get("userId").toString());
                msg.put("duration",jsonObject.get("duration")==null?"0":jsonObject.get("duration").toString());

                umsMessageService = applicationContext.getBean(UmsMessageService.class);
                umsFriendsService = applicationContext.getBean(UmsFriendsService.class);
                //解析发送的报文
                //JSONObject jsonObject = JSON.parseObject(message);
                //追加发送人(防止串改)
                String toUserId=jsonObject.getString("friendId");
                String messageText=jsonObject.getString("content");
                Integer messageType= MallEnum.getkey(jsonObject.getString("type"));
                //判断是否是好友
                boolean isFriends= umsFriendsService.isFriend(this.userId,toUserId);
                if(!isFriends){ return; }
                SimpleDateFormat sfs=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
                UmsMessage umsMessage=new UmsMessage();
                umsMessage.setCreatetime(DateTime.now().toString(sfs));
                umsMessage.setFromuserid(jsonObject.get("userId").toString());
                umsMessage.setTouserid(toUserId);
                umsMessage.setMessagetext(messageText);
                umsMessage.setMessagetype(messageType);
                umsMessage.setReadstatus(0);
                umsMessage.setAuditstatus(0);
                umsMessage.setDuration(jsonObject.get("duration")==null?0:jsonObject.getInteger("duration"));
                //传送给对应toUserId用户的websocket
                if(StringUtils.isNotBlank(toUserId)&&webSocketMap.containsKey(toUserId)){
                    umsMessage.setSendstatus(1);
                    //webSocketMap.get(toUserId).sendMessage(JSONObject.toJSONString(umsMessage));
                    webSocketMap.get(jsonObject.get("friendId").toString()).sendMessage(JSONObject.toJSONString(msg));
                }else{
                    log.error("请求的userId:"+toUserId+"不在该服务器上");
                    //否则不在这个服务器上，发送到mysql或者redis
                    umsMessage.setSendstatus(0);
                }
                umsMessageService.addMessage(umsMessage);

            }catch (Exception e){
                e.printStackTrace();
            }
        }





    }

    /**
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("用户错误:"+this.userId+",原因:"+error.getMessage());
        error.printStackTrace();
    }
    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }


    /**
     * 发送自定义消息
     * */
    public static void sendInfo(String message,@PathParam("userId") String userId) throws IOException {
        log.info("发送消息到:"+userId+"，报文:"+message);
        if(StringUtils.isNotBlank(userId)&&webSocketMap.containsKey(userId)){
            webSocketMap.get(userId).sendMessage(message);
        }else{
            log.error("用户"+userId+",不在线！");
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }
}