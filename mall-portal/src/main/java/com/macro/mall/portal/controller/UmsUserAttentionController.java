package com.macro.mall.portal.controller;

import cn.hutool.core.date.DateTime;
import com.macro.mall.common.api.CommonPage;
import com.macro.mall.common.api.CommonResult;
import com.macro.mall.model.UmsReply;
import com.macro.mall.model.UmsUserAttention;
import com.macro.mall.portal.service.UmsMemberService;
import com.macro.mall.portal.service.UmsUserAttentionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;


@Controller
@Api(tags = "UmsUserAttentionController", description = "我的关注管理")
@RequestMapping("/userattention")
public class UmsUserAttentionController {

    @Autowired
    private UmsMemberService umsMemberService;
    @Autowired
    private UmsUserAttentionService umsUserAttentionService;

    @ApiOperation("添加关注")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult add(@RequestParam("fuserId") String fuserId){
        String userId=umsMemberService.getCurrentUser().getUserid();
        UmsUserAttention umsUserAttention=new UmsUserAttention();
        umsUserAttention.setUserid(userId);
        umsUserAttention.setFuserid(fuserId);
        umsUserAttention.setUserattentiondelstatus(0);
        umsUserAttention.setUserattentioncreatetime(DateTime.now().toString());
        umsUserAttentionService.add(umsUserAttention);
        return CommonResult.success(0);
    }

    @ApiOperation("取消关注")
    @RequestMapping(value = "/cancel", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult cancel(@RequestParam("fuserId") String fuserId){
        String userId=umsMemberService.getCurrentUser().getUserid();
        umsUserAttentionService.cancel(userId,fuserId);
        return CommonResult.success(0);
    }

    @ApiOperation("我的关注")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult list(@RequestParam("pageIndex") Integer pageIndex,@RequestParam("pageSize") Integer pageSize){
        String userId=umsMemberService.getCurrentUser().getUserid();
        List<UmsUserAttention> umsUserAttentionList=umsUserAttentionService.list(userId,pageIndex,pageSize);
        CommonPage<UmsUserAttention> commonPage= CommonPage.restPage(umsUserAttentionList);
        return CommonResult.success(commonPage);
    }

}
