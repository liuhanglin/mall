package com.macro.mall.portal.dao;

import com.macro.mall.portal.dto.CommentDto;

import java.util.List;

public interface ReplyDao {
    List<CommentDto> getTopicCommentReplyTotalCount(Integer topicId, List<Integer> commentId);
}

