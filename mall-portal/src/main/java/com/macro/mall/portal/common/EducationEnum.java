package com.macro.mall.portal.common;

public enum EducationEnum {

    GAOZHONG(0,"高中"),
    BENKE(1,"本科"),
    YANJIUSHENG(2,"研究生"),
    BOSHI(3,"博士");

    private final Integer key;

    private final String value;

    private EducationEnum(final Integer key,final String value){
        this.key = key;
        this.value = value;
    }

    public static Integer getkey(String value) {
        EducationEnum[] businessModeEnums = values();
        for (EducationEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.value().equals(value)) {
                return businessModeEnum.key();
            }
        }
        return null;
    }

    public static String getValue(Integer key) {
        EducationEnum[] businessModeEnums = values();
        for (EducationEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.key().equals(key)) {
                return businessModeEnum.value();
            }
        }
        return null;
    }


    public Integer key(){
        return this.key;
    }

    public String value(){
        return this.value;
    }
}
