package com.macro.mall.portal.common;

public enum MarriedEnum {
    WEIHUN(0,"未婚"),
    YIHUN(1,"已婚"),
    LIYI(2,"离异")
    ;

    private final Integer key;

    private final String value;

    private MarriedEnum(final Integer key,final String value){
        this.key = key;
        this.value = value;
    }

    public static Integer getkey(String value) {
        MarriedEnum[] businessModeEnums = values();
        for (MarriedEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.value().equals(value)) {
                return businessModeEnum.key();
            }
        }
        return null;
    }

    public static String getValue(Integer key) {
        MarriedEnum[] businessModeEnums = values();
        for (MarriedEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.key().equals(key)) {
                return businessModeEnum.value();
            }
        }
        return null;
    }


    public Integer key(){
        return this.key;
    }

    public String value(){
        return this.value;
    }
}
