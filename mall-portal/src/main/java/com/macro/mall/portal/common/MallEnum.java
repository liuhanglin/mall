package com.macro.mall.portal.common;

public enum  MallEnum {
    TEXT(0,"text"),

    IMAGE(1,"image"),
    VOICE(2,"voice")
    ;



    private final Integer key;

    private final String value;

    private MallEnum(final Integer key,final String value){
        this.key = key;
        this.value = value;
    }

    public static Integer getkey(String value) {
        MallEnum[] businessModeEnums = values();
        for (MallEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.value().equals(value)) {
                return businessModeEnum.key();
            }
        }
        return null;
    }

    public static String getValue(Integer key) {
        MallEnum[] businessModeEnums = values();
        for (MallEnum businessModeEnum : businessModeEnums) {
            if (businessModeEnum.key().equals(key)) {
                return businessModeEnum.value();
            }
        }
        return null;
    }


    public Integer key(){
        return this.key;
    }

    public String value(){
        return this.value;
    }
}
