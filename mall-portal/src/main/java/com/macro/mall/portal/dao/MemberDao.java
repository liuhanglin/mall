package com.macro.mall.portal.dao;

import com.macro.mall.model.UmsUser;
import com.macro.mall.portal.dto.UserDetailDto;

import java.util.List;

public interface MemberDao {
    List<UserDetailDto> getUserDetail(List<String> userIds);
}
