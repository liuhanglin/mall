package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReplyDto {
    private Integer topicId;
    private Integer commentId;
    private String replyContent;
    private String userId;
    private String nickName;
    private String avatorUrl;
    private String replyCreatetime;
    private Integer replyPraisecount;
    private Integer replyAuditstatus;
    private Integer parentReplyid;
    private String parentUserid;
    private String parentNickname;
    private String parentAvatorurl;
}
