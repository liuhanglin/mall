package com.macro.mall.portal.service;

import com.macro.mall.model.UmsUserAttention;

import java.util.List;

public interface UmsUserAttentionService {
    Integer add(UmsUserAttention userAttention);
    Integer cancel(String userId,String fuserId);
    List<UmsUserAttention> list(String userId,Integer pageIndex,Integer pageSize);
}
