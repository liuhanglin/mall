package com.macro.mall.portal.dao;

import com.macro.mall.portal.dto.TopicDetailDto;
import com.macro.mall.portal.dto.TopicIndexDto;

import java.util.List;

public interface TopicDao {
    List<TopicIndexDto> getIndexShowTopicList();
    TopicDetailDto getTopicDetail(Integer topicId,Integer topicDelstatus,Integer topicAuditstatus,Integer topicPublishstatus);
    Integer updateBrowsecount(Integer topicId,Integer count);
    Integer updatePraisecount(Integer topicId,Integer count);
    String getTopicUserid(Integer topicId);
}
