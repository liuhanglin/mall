package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddCommentDto {
    private Integer topicId;
    private String commentContent;
    private Integer parentCommentid;
}
