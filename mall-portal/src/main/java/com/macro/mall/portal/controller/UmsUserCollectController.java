package com.macro.mall.portal.controller;

import cn.hutool.core.date.DateUtil;
import com.macro.mall.common.api.CommonPage;
import com.macro.mall.common.api.CommonResult;
import com.macro.mall.model.UmsUserAttention;
import com.macro.mall.model.UmsUserCollect;
import com.macro.mall.portal.service.UmsMemberService;
import com.macro.mall.portal.service.UmsUserCollectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@Api(tags = "UmsUserCollectController", description = "我的收藏管理")
@RequestMapping("/usercollect")
public class UmsUserCollectController {

    @Autowired
    private UmsMemberService umsMemberService;
    @Autowired
    private UmsUserCollectionService umsUserCollectionService;

    @ApiOperation("添加收藏")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult add(@RequestParam("fuserId") String fuserId,@RequestParam("topicId") Integer topicId){

        UmsUserCollect umsUserCollect=new UmsUserCollect();
        umsUserCollect.setTopicid(topicId);
        umsUserCollect.setUserid(fuserId);
        umsUserCollect.setUsercollectdelstatus(0);
        umsUserCollect.setUsercollectcreatetime(DateUtil.now().toString());
        umsUserCollectionService.add(umsUserCollect);
        return CommonResult.success(0);
    }

    @ApiOperation("取消收藏")
    @RequestMapping(value = "/cancel", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult cancel(@RequestParam("fuserId") String fuserId,@RequestParam("topicId") Integer topicId){
        umsUserCollectionService.cancel(fuserId,topicId);
        return CommonResult.success(0);
    }

    @ApiOperation("我的收藏")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult list(@RequestParam("pageIndex") Integer pageIndex,@RequestParam("pageSize") Integer pageSize){
        String userId=umsMemberService.getCurrentUser().getUserid();
        List<UmsUserCollect> umsUserCollectList=umsUserCollectionService.list(userId,pageIndex,pageSize);
        CommonPage<UmsUserCollect> commonPage= CommonPage.restPage(umsUserCollectList);
        return CommonResult.success(commonPage);
    }
}
