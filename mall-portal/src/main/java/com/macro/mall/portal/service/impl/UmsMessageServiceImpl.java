package com.macro.mall.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.UmsMessageMapper;
import com.macro.mall.model.UmsMessage;
import com.macro.mall.model.UmsMessageExample;
import com.macro.mall.portal.dao.MessageDao;
import com.macro.mall.portal.domain.UserMessageDetail;
import com.macro.mall.portal.dto.UserNoReadCoutDto;
import com.macro.mall.portal.service.UmsMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsMessageServiceImpl implements UmsMessageService {

    @Autowired
    private UmsMessageMapper umsMessageMapper;

    @Autowired
    private MessageDao messageDao;

    @Override
    public boolean addMessage(UmsMessage umsMessage) {
        Integer res= umsMessageMapper.insert(umsMessage);
        return res>0;
    }

    @Override
    public List<UmsMessage> getMessage(UmsMessage umsMessage) {
        UmsMessageExample umsMessageExample=new UmsMessageExample();
        return null;
    }

    @Override
    public List<UmsMessage> getNoSendMessage(String touserId) {
        UmsMessageExample umsMessageExample=new UmsMessageExample();
        umsMessageExample.createCriteria().
                andTouseridEqualTo(touserId)
                .andSendstatusEqualTo(0);
        List<UmsMessage> umsMessageList=umsMessageMapper.selectByExample(umsMessageExample);
        return umsMessageList;
    }

    @Override
    public Boolean updateMessageSendStatus(List<Integer> messageIds) {
        Integer res= messageDao.updateSendStatusByBatch(messageIds);
        return res>0;
    }

    @Override
    public List<UserNoReadCoutDto> getUserNoReadCountList(List<String> fromuserIds, String userId) {
        return messageDao.getUserNoReadCountList(fromuserIds,userId);
    }

    @Override
    public Integer getUserNoReadMessageCount(String userId) {
        return messageDao.getUserNoReadMessageCount(userId);
    }

    @Override
    public List<UmsMessage> getUserLastMessage(List<String> fromuserIds, String userId) {
        return messageDao.getUserLastMessage(fromuserIds,userId);
    }

    @Override
    public List<UmsMessage> getHistoryMessage(String userId, String friendId,Integer pageIndex,Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        return messageDao.getHistoryMessage(userId,friendId);
    }

    @Override
    public Integer updateMessageReadStatus(String userId, String friendId) {
        return messageDao.updateMessageReadStatus(userId,friendId);
    }
}
