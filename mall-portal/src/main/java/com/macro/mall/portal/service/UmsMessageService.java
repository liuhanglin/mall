package com.macro.mall.portal.service;

import com.macro.mall.model.UmsMessage;
import com.macro.mall.portal.domain.UserMessageDetail;
import com.macro.mall.portal.dto.UserNoReadCoutDto;

import java.util.List;

public interface UmsMessageService {
    boolean addMessage(UmsMessage umsMessage);

    List<UmsMessage> getMessage(UmsMessage umsMessage);

    List<UmsMessage> getNoSendMessage(String touserId);

    Boolean updateMessageSendStatus(List<Integer> messageIds);

    List<UserNoReadCoutDto> getUserNoReadCountList(List<String> fromuserIds, String userId);

    Integer getUserNoReadMessageCount(String userId);

    List<UmsMessage> getUserLastMessage(List<String> fromuserIds, String userId);

    List<UmsMessage> getHistoryMessage(String userId,String friendId,Integer pageIndex,Integer pageSize);

    Integer updateMessageReadStatus(String userId,String friendId);
}
