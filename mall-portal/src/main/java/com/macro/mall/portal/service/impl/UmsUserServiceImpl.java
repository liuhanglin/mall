package com.macro.mall.portal.service.impl;

import com.macro.mall.mapper.UmsUserMapper;
import com.macro.mall.model.UmsUser;
import com.macro.mall.model.UmsUserExample;
import com.macro.mall.portal.service.UmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmsUserServiceImpl implements UmsUserService {


    @Autowired
    private UmsUserMapper umsUserMapper;

    @Override
    public Integer addUser(UmsUser umsUser) {
        return umsUserMapper.insertSelective(umsUser);
    }

    @Override
    public UmsUser findUserByopenid(String openId) {
        UmsUserExample umsUserExample=new UmsUserExample();
        umsUserExample.createCriteria().andOpenidEqualTo(openId);
        List<UmsUser> umsUsers=umsUserMapper.selectByExample(umsUserExample);
        return umsUsers.size()>0?umsUsers.get(0):null;
    }
}
