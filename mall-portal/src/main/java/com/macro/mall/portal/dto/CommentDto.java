package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDto {
    private Integer topicId;
    private Integer commentId;
    private String userId;
    private String nickName;
    private String avatorUrl;
    private Integer gender;
    private String commentContent;
    private String commentCreatetime;
    private Integer commentTopstatus;
    private Integer commentAuditstatus;
    private Integer replyTotalcount;
    private Integer commentPraisecount;
    private Integer parentCommentid;
    private String parentUserId;
    private String parentNickName;
    private String parentAvatorurl;
    private String parentCommentcontent;
    private Integer parentGender;
    private Boolean isAuthor;
    private Boolean isPraise;
}
