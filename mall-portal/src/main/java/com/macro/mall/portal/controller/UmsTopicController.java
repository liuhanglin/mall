package com.macro.mall.portal.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.macro.mall.common.api.CommonPage;
import com.macro.mall.common.api.CommonResult;
import com.macro.mall.model.UmsTopic;
import com.macro.mall.model.UmsTopicImage;
import com.macro.mall.portal.common.EducationEnum;
import com.macro.mall.portal.common.GenderEnum;
import com.macro.mall.portal.common.MallConst;
import com.macro.mall.portal.common.MarriedEnum;
import com.macro.mall.portal.dto.TopicDetailDto;
import com.macro.mall.portal.dto.TopicIndexDto;
import com.macro.mall.portal.dto.UserNoReadCoutDto;
import com.macro.mall.portal.service.UmsCommentService;
import com.macro.mall.portal.service.UmsMemberService;
import com.macro.mall.portal.service.UmsTopicImageService;
import com.macro.mall.portal.service.UmsTopicService;
import com.macro.mall.security.service.RedisService;
import com.macro.mall.security.service.impl.RedisServiceImplExtend;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@Api(tags = "UmsTopicController", description = "会员话题管理")
@RequestMapping("/topic")
public class UmsTopicController {

    @Autowired
    private UmsTopicService umsTopicService;
    @Autowired
    private UmsTopicImageService umsTopicImageService;
    @Autowired
    private UmsMemberService umsMemberService;
    @Autowired
    private UmsCommentService umsCommentService;

    @Autowired
    private RedisServiceImplExtend redisServiceImplExtend;

    @Autowired
    private RedisService redisService;

    @ApiOperation("获取首页展示的会员话题列表")
    @RequestMapping(value = "/index/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getIndexTopicList( @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                   @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex) {
        List<TopicIndexDto> topicIndexDtoList= umsTopicService.getIndexShowTopicList(pageIndex,pageSize);
        return CommonResult.success(CommonPage.restPage(topicIndexDtoList));
    }


    @ApiOperation("获取会员话题详情")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getTopicDetail(@RequestParam(value = "topicId") Integer topicId
                                        ,@RequestParam(value = "topicPublishstatus") Integer topicPublishstatus) {
        //获取话题详情
        TopicDetailDto topicDetailDto =umsTopicService.getTopicDetail(topicId,0,1,topicPublishstatus==-1?null:topicPublishstatus);
        if (topicDetailDto==null)     return CommonResult.success(null);
        //获取话题图片
        List<UmsTopicImage> umsTopicImageList= umsTopicImageService.getTopicImageList(topicId,1,0);
        List<String> images= umsTopicImageList.stream().map(UmsTopicImage::getTopicimageurl).collect(Collectors.toList());
        if(images.size()>0) {
            topicDetailDto.setTopicImages(images);
        }
        //获取话题浏览量
        Object browseCountObj= redisService.hGet(MallConst.RKEY_COMMENT_BROWSECOUNT+"BROWSECOUNT",topicId.toString());
        topicDetailDto.setTopicBrowsecount(browseCountObj==null?0:(Integer)browseCountObj);
        //获取话题点赞数
        Long praiseCount= redisServiceImplExtend.bitCount(MallConst.RKEY_TOPIC_PARISECOUNT+topicId);
        topicDetailDto.setTopicPraisecount(Integer.parseInt(praiseCount.toString()));
        //获取话题评论数
        Long commentCount= umsCommentService.getTopicCommentCount(topicId);
        topicDetailDto.setTopicCommentcount(Integer.parseInt(commentCount.toString()));
        topicDetailDto.setTopicEducationstr(EducationEnum.getValue(topicDetailDto.getTopicEducation()));
        topicDetailDto.setTopicGendertypestr(GenderEnum.getValue(topicDetailDto.getTopicGendertype()));
        topicDetailDto.setTopicMarriedstr(MarriedEnum.getValue(topicDetailDto.getTopicMarried()));
        return CommonResult.success(topicDetailDto);
    }

    @ApiOperation("增加话题浏览量")
    @RequestMapping(value = "/browsecount/incr", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult incrTopicBrowseCount( @RequestParam(value = "topicId") Integer topicId) {
        redisService.hIncr(MallConst.RKEY_COMMENT_BROWSECOUNT+"BROWSECOUNT",topicId.toString(),1L);
        return CommonResult.success(0);
    }

    @ApiOperation("添加会员话题详情")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult addTopicDetail(@RequestBody TopicDetailDto topicDetailDto) {
        //添加到话题表
        String userId=umsMemberService.getCurrentUser().getUserid();
        String datetime=DateUtil.formatDateTime(new Date());
        UmsTopic umsTopic =new UmsTopic();
        umsTopic.setTopicbrowsecount(0);
        umsTopic.setTopicauditstatus(1);
        umsTopic.setTopiccover(topicDetailDto.getTopicCover());
        umsTopic.setTopiccreatetime(datetime);
        umsTopic.setTopicdelstatus(0);
        umsTopic.setTopicfacescore(0);
        umsTopic.setTopictags(topicDetailDto.getTopicTags());
        umsTopic.setTopicmodifytime(datetime);
        umsTopic.setTopicpublishstatus(0);
        if(topicDetailDto.getTopicTemplatestatus()==0) {
            umsTopic.setTopiccontent(topicDetailDto.getTopicContent());
            umsTopic.setTopictitle(topicDetailDto.getTopicTitle());
        }else {
            umsTopic.setTopicdemand(topicDetailDto.getTopicDemand());
            umsTopic.setTopiceducation(topicDetailDto.getTopicEducation());
            umsTopic.setTopicfacescore(topicDetailDto.getTopicFacescore());
            umsTopic.setTopicgendertype(topicDetailDto.getTopicGendertype());
            umsTopic.setTopicheight(topicDetailDto.getTopicHeight());
            umsTopic.setTopicinterest(topicDetailDto.getTopicInterest());
            umsTopic.setTopicmarried(topicDetailDto.getTopicMarried());
            umsTopic.setTopicselfintroduction(topicDetailDto.getTopicSelfintroduction());
            umsTopic.setTopicweight(topicDetailDto.getTopicWeight());
            umsTopic.setTopicareacode(topicDetailDto.getTopicAreacode());
            umsTopic.setTopicage(topicDetailDto.getTopicAge());
        }
        umsTopic.setUserid(userId);
        umsTopic.setTopictopstatus(0);
        umsTopic.setTopictemplatestatus(topicDetailDto.getTopicTemplatestatus());
        umsTopic.setTopictags(topicDetailDto.getTopicTags());
        umsTopicService.insertSelective(umsTopic);
        //添加到话题图片表
        List<UmsTopicImage> umsTopicImageList=new ArrayList<UmsTopicImage>();
        for (String imageUrl : topicDetailDto.getTopicImages()){
            UmsTopicImage umsTopicImage=new UmsTopicImage();
            umsTopicImage.setTopicid(umsTopic.getTopicid());
            umsTopicImage.setTopicimageauditstatus(1);
            umsTopicImage.setTopicimagedelstatus(0);
            umsTopicImage.setTopicimageurl(imageUrl);
            umsTopicImage.setTopicimagecreatetime(datetime);
            umsTopicImageList.add(umsTopicImage);
        }
        umsTopicImageService.batchInsertTopicimage(umsTopicImageList);
        return CommonResult.success(0);
    }

    @ApiOperation("修改会员话题详情")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult updateTopicDetail(@RequestBody TopicDetailDto topicDetailDto) {
        String datetime=DateUtil.formatDateTime(new Date());
        UmsTopic umsTopic =new UmsTopic();
        if(topicDetailDto.getTopicTemplatestatus()==0) {
            umsTopic.setTopiccontent(topicDetailDto.getTopicContent());
            umsTopic.setTopictitle(topicDetailDto.getTopicTitle());
        }else {
            umsTopic.setTopicdemand(topicDetailDto.getTopicDemand());
            umsTopic.setTopiceducation(topicDetailDto.getTopicEducation());
            umsTopic.setTopicfacescore(topicDetailDto.getTopicFacescore());
            umsTopic.setTopicgendertype(topicDetailDto.getTopicGendertype());
            umsTopic.setTopicheight(topicDetailDto.getTopicHeight());
            umsTopic.setTopicinterest(topicDetailDto.getTopicInterest());
            umsTopic.setTopicmarried(topicDetailDto.getTopicMarried());
            umsTopic.setTopicselfintroduction(topicDetailDto.getTopicSelfintroduction());
            umsTopic.setTopicweight(topicDetailDto.getTopicWeight());
            umsTopic.setTopicareacode(topicDetailDto.getTopicAreacode());
            umsTopic.setTopicage(topicDetailDto.getTopicAge());
        }
        umsTopic.setTopicmodifytime(datetime);
        umsTopic.setTopictemplatestatus(topicDetailDto.getTopicTemplatestatus());
        umsTopic.setTopictags(topicDetailDto.getTopicTags());
        umsTopicService.updateSelective(umsTopic);

        //删除图片
        umsTopicImageService.deleteTopicimage(topicDetailDto.getTopicId());
        //添加到话题图片表
        List<UmsTopicImage> umsTopicImageList=new ArrayList<UmsTopicImage>();
        for (String imageUrl : topicDetailDto.getTopicImages()){
            UmsTopicImage umsTopicImage=new UmsTopicImage();
            umsTopicImage.setTopicid(umsTopic.getTopicid());
            umsTopicImage.setTopicimageauditstatus(1);
            umsTopicImage.setTopicimagedelstatus(0);
            umsTopicImage.setTopicimageurl(imageUrl);
            umsTopicImage.setTopicimagecreatetime(datetime);
            umsTopicImageList.add(umsTopicImage);
        }
        umsTopicImageService.batchInsertTopicimage(umsTopicImageList);
        return CommonResult.success(0);
    }

    @ApiOperation("修改会员话题模板状态")
    @RequestMapping(value = "/update/templatestatus", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult updateTopictemplatestatus(@RequestParam("topicId") Integer topicId,
                                                 @RequestParam("status") Integer status) {
        UmsTopic umsTopic =new UmsTopic();
        umsTopic.setTopicid(topicId);
        umsTopic.setTopictemplatestatus(status);
        umsTopicService.updateSelective(umsTopic);
        return CommonResult.success(0);
    }

    @ApiOperation("修改会员话题模板状态")
    @RequestMapping(value = "/update/publishstatus", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult updateTopicPublishStatus(@RequestParam("topicId") Integer topicId,
                                                 @RequestParam("status") Integer status) {
        UmsTopic umsTopic =new UmsTopic();
        umsTopic.setTopicid(topicId);
        umsTopic.setTopicpublishstatus(status);
        umsTopicService.updateSelective(umsTopic);
        return CommonResult.success(0);
    }


    @ApiOperation("添加赞")
    @RequestMapping(value = "/parise/add", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult addParise(@RequestParam("topicId") Integer topicId) {
        Integer uId=umsMemberService.getCurrentUser().getId();
        redisServiceImplExtend.setBit(MallConst.RKEY_TOPIC_PARISECOUNT+topicId,uId,true);
        return CommonResult.success(0);
    }

    @ApiOperation("取消赞")
    @RequestMapping(value = "/parise/cancel", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult cancelParise(@RequestParam("topicId") Integer topicId) {
        Integer uId=umsMemberService.getCurrentUser().getId();
        redisServiceImplExtend.setBit(MallConst.RKEY_TOPIC_PARISECOUNT+topicId,uId,false);
        return CommonResult.success(0);
    }


}

