package com.macro.mall.portal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailDto {
    private Integer id;
    private String userId;
    private String memberId;
    private String nickName;
    private String avatorUrl;
    private String createTime;
    private Integer gender;
}
