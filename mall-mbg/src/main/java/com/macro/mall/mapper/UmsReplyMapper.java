package com.macro.mall.mapper;

import com.macro.mall.model.UmsReply;
import com.macro.mall.model.UmsReplyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsReplyMapper {
    long countByExample(UmsReplyExample example);

    int deleteByExample(UmsReplyExample example);

    int deleteByPrimaryKey(Integer replyid);

    int insert(UmsReply record);

    int insertSelective(UmsReply record);

    List<UmsReply> selectByExample(UmsReplyExample example);

    UmsReply selectByPrimaryKey(Integer replyid);

    int updateByExampleSelective(@Param("record") UmsReply record, @Param("example") UmsReplyExample example);

    int updateByExample(@Param("record") UmsReply record, @Param("example") UmsReplyExample example);

    int updateByPrimaryKeySelective(UmsReply record);

    int updateByPrimaryKey(UmsReply record);
}