package com.macro.mall.mapper;

import com.macro.mall.model.UmsUserCollect;
import com.macro.mall.model.UmsUserCollectExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsUserCollectMapper {
    long countByExample(UmsUserCollectExample example);

    int deleteByExample(UmsUserCollectExample example);

    int deleteByPrimaryKey(Integer usercollectid);

    int insert(UmsUserCollect record);

    int insertSelective(UmsUserCollect record);

    List<UmsUserCollect> selectByExample(UmsUserCollectExample example);

    UmsUserCollect selectByPrimaryKey(Integer usercollectid);

    int updateByExampleSelective(@Param("record") UmsUserCollect record, @Param("example") UmsUserCollectExample example);

    int updateByExample(@Param("record") UmsUserCollect record, @Param("example") UmsUserCollectExample example);

    int updateByPrimaryKeySelective(UmsUserCollect record);

    int updateByPrimaryKey(UmsUserCollect record);
}