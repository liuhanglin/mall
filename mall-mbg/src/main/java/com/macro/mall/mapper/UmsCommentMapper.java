package com.macro.mall.mapper;

import com.macro.mall.model.UmsComment;
import com.macro.mall.model.UmsCommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsCommentMapper {
    long countByExample(UmsCommentExample example);

    int deleteByExample(UmsCommentExample example);

    int deleteByPrimaryKey(Integer commentid);

    int insert(UmsComment record);

    int insertSelective(UmsComment record);

    List<UmsComment> selectByExample(UmsCommentExample example);

    UmsComment selectByPrimaryKey(Integer commentid);

    int updateByExampleSelective(@Param("record") UmsComment record, @Param("example") UmsCommentExample example);

    int updateByExample(@Param("record") UmsComment record, @Param("example") UmsCommentExample example);

    int updateByPrimaryKeySelective(UmsComment record);

    int updateByPrimaryKey(UmsComment record);
}