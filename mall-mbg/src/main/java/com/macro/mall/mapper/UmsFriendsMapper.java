package com.macro.mall.mapper;

import com.macro.mall.model.UmsFriends;
import com.macro.mall.model.UmsFriendsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsFriendsMapper {
    long countByExample(UmsFriendsExample example);

    int deleteByExample(UmsFriendsExample example);

    int deleteByPrimaryKey(String friendid);

    int insert(UmsFriends record);

    int insertSelective(UmsFriends record);

    List<UmsFriends> selectByExample(UmsFriendsExample example);

    UmsFriends selectByPrimaryKey(String friendid);

    int updateByExampleSelective(@Param("record") UmsFriends record, @Param("example") UmsFriendsExample example);

    int updateByExample(@Param("record") UmsFriends record, @Param("example") UmsFriendsExample example);

    int updateByPrimaryKeySelective(UmsFriends record);

    int updateByPrimaryKey(UmsFriends record);
}