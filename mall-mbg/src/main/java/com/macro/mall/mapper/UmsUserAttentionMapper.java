package com.macro.mall.mapper;

import com.macro.mall.model.UmsUserAttention;
import com.macro.mall.model.UmsUserAttentionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsUserAttentionMapper {
    long countByExample(UmsUserAttentionExample example);

    int deleteByExample(UmsUserAttentionExample example);

    int deleteByPrimaryKey(Integer userattentionid);

    int insert(UmsUserAttention record);

    int insertSelective(UmsUserAttention record);

    List<UmsUserAttention> selectByExample(UmsUserAttentionExample example);

    UmsUserAttention selectByPrimaryKey(Integer userattentionid);

    int updateByExampleSelective(@Param("record") UmsUserAttention record, @Param("example") UmsUserAttentionExample example);

    int updateByExample(@Param("record") UmsUserAttention record, @Param("example") UmsUserAttentionExample example);

    int updateByPrimaryKeySelective(UmsUserAttention record);

    int updateByPrimaryKey(UmsUserAttention record);
}