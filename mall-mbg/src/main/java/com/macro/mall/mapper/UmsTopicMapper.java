package com.macro.mall.mapper;

import com.macro.mall.model.UmsTopic;
import com.macro.mall.model.UmsTopicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsTopicMapper {
    long countByExample(UmsTopicExample example);

    int deleteByExample(UmsTopicExample example);

    int deleteByPrimaryKey(Integer topicid);

    int insert(UmsTopic record);

    int insertSelective(UmsTopic record);

    List<UmsTopic> selectByExampleWithBLOBs(UmsTopicExample example);

    List<UmsTopic> selectByExample(UmsTopicExample example);

    UmsTopic selectByPrimaryKey(Integer topicid);

    int updateByExampleSelective(@Param("record") UmsTopic record, @Param("example") UmsTopicExample example);

    int updateByExampleWithBLOBs(@Param("record") UmsTopic record, @Param("example") UmsTopicExample example);

    int updateByExample(@Param("record") UmsTopic record, @Param("example") UmsTopicExample example);

    int updateByPrimaryKeySelective(UmsTopic record);

    int updateByPrimaryKeyWithBLOBs(UmsTopic record);

    int updateByPrimaryKey(UmsTopic record);
}