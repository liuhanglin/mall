package com.macro.mall.mapper;

import com.macro.mall.model.UmsTopicImage;
import com.macro.mall.model.UmsTopicImageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsTopicImageMapper {
    long countByExample(UmsTopicImageExample example);

    int deleteByExample(UmsTopicImageExample example);

    int deleteByPrimaryKey(Integer topicimageid);

    int insert(UmsTopicImage record);

    int insertSelective(UmsTopicImage record);

    List<UmsTopicImage> selectByExample(UmsTopicImageExample example);

    UmsTopicImage selectByPrimaryKey(Integer topicimageid);

    int updateByExampleSelective(@Param("record") UmsTopicImage record, @Param("example") UmsTopicImageExample example);

    int updateByExample(@Param("record") UmsTopicImage record, @Param("example") UmsTopicImageExample example);

    int updateByPrimaryKeySelective(UmsTopicImage record);

    int updateByPrimaryKey(UmsTopicImage record);
}