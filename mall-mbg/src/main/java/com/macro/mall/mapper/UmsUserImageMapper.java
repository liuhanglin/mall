package com.macro.mall.mapper;

import com.macro.mall.model.UmsUserImage;
import com.macro.mall.model.UmsUserImageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsUserImageMapper {
    long countByExample(UmsUserImageExample example);

    int deleteByExample(UmsUserImageExample example);

    int deleteByPrimaryKey(Integer userimageid);

    int insert(UmsUserImage record);

    int insertSelective(UmsUserImage record);

    List<UmsUserImage> selectByExample(UmsUserImageExample example);

    UmsUserImage selectByPrimaryKey(Integer userimageid);

    int updateByExampleSelective(@Param("record") UmsUserImage record, @Param("example") UmsUserImageExample example);

    int updateByExample(@Param("record") UmsUserImage record, @Param("example") UmsUserImageExample example);

    int updateByPrimaryKeySelective(UmsUserImage record);

    int updateByPrimaryKey(UmsUserImage record);
}