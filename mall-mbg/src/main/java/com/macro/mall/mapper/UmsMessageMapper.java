package com.macro.mall.mapper;

import com.macro.mall.model.UmsMessage;
import com.macro.mall.model.UmsMessageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsMessageMapper {
    long countByExample(UmsMessageExample example);

    int deleteByExample(UmsMessageExample example);

    int deleteByPrimaryKey(Integer messageid);

    int insert(UmsMessage record);

    int insertSelective(UmsMessage record);

    List<UmsMessage> selectByExample(UmsMessageExample example);

    UmsMessage selectByPrimaryKey(Integer messageid);

    int updateByExampleSelective(@Param("record") UmsMessage record, @Param("example") UmsMessageExample example);

    int updateByExample(@Param("record") UmsMessage record, @Param("example") UmsMessageExample example);

    int updateByPrimaryKeySelective(UmsMessage record);

    int updateByPrimaryKey(UmsMessage record);
}