package com.macro.mall.model;

import java.util.ArrayList;
import java.util.List;

public class UmsReplyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UmsReplyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andReplyidIsNull() {
            addCriterion("replyId is null");
            return (Criteria) this;
        }

        public Criteria andReplyidIsNotNull() {
            addCriterion("replyId is not null");
            return (Criteria) this;
        }

        public Criteria andReplyidEqualTo(Integer value) {
            addCriterion("replyId =", value, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidNotEqualTo(Integer value) {
            addCriterion("replyId <>", value, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidGreaterThan(Integer value) {
            addCriterion("replyId >", value, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidGreaterThanOrEqualTo(Integer value) {
            addCriterion("replyId >=", value, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidLessThan(Integer value) {
            addCriterion("replyId <", value, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidLessThanOrEqualTo(Integer value) {
            addCriterion("replyId <=", value, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidIn(List<Integer> values) {
            addCriterion("replyId in", values, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidNotIn(List<Integer> values) {
            addCriterion("replyId not in", values, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidBetween(Integer value1, Integer value2) {
            addCriterion("replyId between", value1, value2, "replyid");
            return (Criteria) this;
        }

        public Criteria andReplyidNotBetween(Integer value1, Integer value2) {
            addCriterion("replyId not between", value1, value2, "replyid");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNull() {
            addCriterion("topicId is null");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNotNull() {
            addCriterion("topicId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicidEqualTo(Integer value) {
            addCriterion("topicId =", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotEqualTo(Integer value) {
            addCriterion("topicId <>", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThan(Integer value) {
            addCriterion("topicId >", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicId >=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThan(Integer value) {
            addCriterion("topicId <", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThanOrEqualTo(Integer value) {
            addCriterion("topicId <=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidIn(List<Integer> values) {
            addCriterion("topicId in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotIn(List<Integer> values) {
            addCriterion("topicId not in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidBetween(Integer value1, Integer value2) {
            addCriterion("topicId between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicId not between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andCommentidIsNull() {
            addCriterion("commentId is null");
            return (Criteria) this;
        }

        public Criteria andCommentidIsNotNull() {
            addCriterion("commentId is not null");
            return (Criteria) this;
        }

        public Criteria andCommentidEqualTo(Integer value) {
            addCriterion("commentId =", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotEqualTo(Integer value) {
            addCriterion("commentId <>", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidGreaterThan(Integer value) {
            addCriterion("commentId >", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentId >=", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidLessThan(Integer value) {
            addCriterion("commentId <", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidLessThanOrEqualTo(Integer value) {
            addCriterion("commentId <=", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidIn(List<Integer> values) {
            addCriterion("commentId in", values, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotIn(List<Integer> values) {
            addCriterion("commentId not in", values, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidBetween(Integer value1, Integer value2) {
            addCriterion("commentId between", value1, value2, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotBetween(Integer value1, Integer value2) {
            addCriterion("commentId not between", value1, value2, "commentid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidIsNull() {
            addCriterion("parentReplyid is null");
            return (Criteria) this;
        }

        public Criteria andParentreplyidIsNotNull() {
            addCriterion("parentReplyid is not null");
            return (Criteria) this;
        }

        public Criteria andParentreplyidEqualTo(Integer value) {
            addCriterion("parentReplyid =", value, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidNotEqualTo(Integer value) {
            addCriterion("parentReplyid <>", value, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidGreaterThan(Integer value) {
            addCriterion("parentReplyid >", value, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidGreaterThanOrEqualTo(Integer value) {
            addCriterion("parentReplyid >=", value, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidLessThan(Integer value) {
            addCriterion("parentReplyid <", value, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidLessThanOrEqualTo(Integer value) {
            addCriterion("parentReplyid <=", value, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidIn(List<Integer> values) {
            addCriterion("parentReplyid in", values, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidNotIn(List<Integer> values) {
            addCriterion("parentReplyid not in", values, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidBetween(Integer value1, Integer value2) {
            addCriterion("parentReplyid between", value1, value2, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentreplyidNotBetween(Integer value1, Integer value2) {
            addCriterion("parentReplyid not between", value1, value2, "parentreplyid");
            return (Criteria) this;
        }

        public Criteria andParentuseridIsNull() {
            addCriterion("parentUserid is null");
            return (Criteria) this;
        }

        public Criteria andParentuseridIsNotNull() {
            addCriterion("parentUserid is not null");
            return (Criteria) this;
        }

        public Criteria andParentuseridEqualTo(String value) {
            addCriterion("parentUserid =", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridNotEqualTo(String value) {
            addCriterion("parentUserid <>", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridGreaterThan(String value) {
            addCriterion("parentUserid >", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridGreaterThanOrEqualTo(String value) {
            addCriterion("parentUserid >=", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridLessThan(String value) {
            addCriterion("parentUserid <", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridLessThanOrEqualTo(String value) {
            addCriterion("parentUserid <=", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridLike(String value) {
            addCriterion("parentUserid like", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridNotLike(String value) {
            addCriterion("parentUserid not like", value, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridIn(List<String> values) {
            addCriterion("parentUserid in", values, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridNotIn(List<String> values) {
            addCriterion("parentUserid not in", values, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridBetween(String value1, String value2) {
            addCriterion("parentUserid between", value1, value2, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andParentuseridNotBetween(String value1, String value2) {
            addCriterion("parentUserid not between", value1, value2, "parentuserid");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountIsNull() {
            addCriterion("replyPraisecount is null");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountIsNotNull() {
            addCriterion("replyPraisecount is not null");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountEqualTo(Integer value) {
            addCriterion("replyPraisecount =", value, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountNotEqualTo(Integer value) {
            addCriterion("replyPraisecount <>", value, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountGreaterThan(Integer value) {
            addCriterion("replyPraisecount >", value, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountGreaterThanOrEqualTo(Integer value) {
            addCriterion("replyPraisecount >=", value, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountLessThan(Integer value) {
            addCriterion("replyPraisecount <", value, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountLessThanOrEqualTo(Integer value) {
            addCriterion("replyPraisecount <=", value, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountIn(List<Integer> values) {
            addCriterion("replyPraisecount in", values, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountNotIn(List<Integer> values) {
            addCriterion("replyPraisecount not in", values, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountBetween(Integer value1, Integer value2) {
            addCriterion("replyPraisecount between", value1, value2, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplypraisecountNotBetween(Integer value1, Integer value2) {
            addCriterion("replyPraisecount not between", value1, value2, "replypraisecount");
            return (Criteria) this;
        }

        public Criteria andReplycontentIsNull() {
            addCriterion("replyContent is null");
            return (Criteria) this;
        }

        public Criteria andReplycontentIsNotNull() {
            addCriterion("replyContent is not null");
            return (Criteria) this;
        }

        public Criteria andReplycontentEqualTo(String value) {
            addCriterion("replyContent =", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentNotEqualTo(String value) {
            addCriterion("replyContent <>", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentGreaterThan(String value) {
            addCriterion("replyContent >", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentGreaterThanOrEqualTo(String value) {
            addCriterion("replyContent >=", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentLessThan(String value) {
            addCriterion("replyContent <", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentLessThanOrEqualTo(String value) {
            addCriterion("replyContent <=", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentLike(String value) {
            addCriterion("replyContent like", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentNotLike(String value) {
            addCriterion("replyContent not like", value, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentIn(List<String> values) {
            addCriterion("replyContent in", values, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentNotIn(List<String> values) {
            addCriterion("replyContent not in", values, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentBetween(String value1, String value2) {
            addCriterion("replyContent between", value1, value2, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycontentNotBetween(String value1, String value2) {
            addCriterion("replyContent not between", value1, value2, "replycontent");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeIsNull() {
            addCriterion("replyCreatetime is null");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeIsNotNull() {
            addCriterion("replyCreatetime is not null");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeEqualTo(String value) {
            addCriterion("replyCreatetime =", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeNotEqualTo(String value) {
            addCriterion("replyCreatetime <>", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeGreaterThan(String value) {
            addCriterion("replyCreatetime >", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("replyCreatetime >=", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeLessThan(String value) {
            addCriterion("replyCreatetime <", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeLessThanOrEqualTo(String value) {
            addCriterion("replyCreatetime <=", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeLike(String value) {
            addCriterion("replyCreatetime like", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeNotLike(String value) {
            addCriterion("replyCreatetime not like", value, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeIn(List<String> values) {
            addCriterion("replyCreatetime in", values, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeNotIn(List<String> values) {
            addCriterion("replyCreatetime not in", values, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeBetween(String value1, String value2) {
            addCriterion("replyCreatetime between", value1, value2, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplycreatetimeNotBetween(String value1, String value2) {
            addCriterion("replyCreatetime not between", value1, value2, "replycreatetime");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusIsNull() {
            addCriterion("replyAuditstatus is null");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusIsNotNull() {
            addCriterion("replyAuditstatus is not null");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusEqualTo(Integer value) {
            addCriterion("replyAuditstatus =", value, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusNotEqualTo(Integer value) {
            addCriterion("replyAuditstatus <>", value, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusGreaterThan(Integer value) {
            addCriterion("replyAuditstatus >", value, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("replyAuditstatus >=", value, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusLessThan(Integer value) {
            addCriterion("replyAuditstatus <", value, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("replyAuditstatus <=", value, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusIn(List<Integer> values) {
            addCriterion("replyAuditstatus in", values, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusNotIn(List<Integer> values) {
            addCriterion("replyAuditstatus not in", values, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusBetween(Integer value1, Integer value2) {
            addCriterion("replyAuditstatus between", value1, value2, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplyauditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("replyAuditstatus not between", value1, value2, "replyauditstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusIsNull() {
            addCriterion("replyDelstatus is null");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusIsNotNull() {
            addCriterion("replyDelstatus is not null");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusEqualTo(Integer value) {
            addCriterion("replyDelstatus =", value, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusNotEqualTo(Integer value) {
            addCriterion("replyDelstatus <>", value, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusGreaterThan(Integer value) {
            addCriterion("replyDelstatus >", value, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("replyDelstatus >=", value, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusLessThan(Integer value) {
            addCriterion("replyDelstatus <", value, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("replyDelstatus <=", value, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusIn(List<Integer> values) {
            addCriterion("replyDelstatus in", values, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusNotIn(List<Integer> values) {
            addCriterion("replyDelstatus not in", values, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusBetween(Integer value1, Integer value2) {
            addCriterion("replyDelstatus between", value1, value2, "replydelstatus");
            return (Criteria) this;
        }

        public Criteria andReplydelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("replyDelstatus not between", value1, value2, "replydelstatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}