package com.macro.mall.model;

import java.util.ArrayList;
import java.util.List;

public class UmsTopicImageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UmsTopicImageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTopicimageidIsNull() {
            addCriterion("topicimageId is null");
            return (Criteria) this;
        }

        public Criteria andTopicimageidIsNotNull() {
            addCriterion("topicimageId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicimageidEqualTo(Integer value) {
            addCriterion("topicimageId =", value, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidNotEqualTo(Integer value) {
            addCriterion("topicimageId <>", value, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidGreaterThan(Integer value) {
            addCriterion("topicimageId >", value, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicimageId >=", value, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidLessThan(Integer value) {
            addCriterion("topicimageId <", value, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidLessThanOrEqualTo(Integer value) {
            addCriterion("topicimageId <=", value, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidIn(List<Integer> values) {
            addCriterion("topicimageId in", values, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidNotIn(List<Integer> values) {
            addCriterion("topicimageId not in", values, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidBetween(Integer value1, Integer value2) {
            addCriterion("topicimageId between", value1, value2, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicimageidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicimageId not between", value1, value2, "topicimageid");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNull() {
            addCriterion("topicId is null");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNotNull() {
            addCriterion("topicId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicidEqualTo(Integer value) {
            addCriterion("topicId =", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotEqualTo(Integer value) {
            addCriterion("topicId <>", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThan(Integer value) {
            addCriterion("topicId >", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicId >=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThan(Integer value) {
            addCriterion("topicId <", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThanOrEqualTo(Integer value) {
            addCriterion("topicId <=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidIn(List<Integer> values) {
            addCriterion("topicId in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotIn(List<Integer> values) {
            addCriterion("topicId not in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidBetween(Integer value1, Integer value2) {
            addCriterion("topicId between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicId not between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlIsNull() {
            addCriterion("topicimageUrl is null");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlIsNotNull() {
            addCriterion("topicimageUrl is not null");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlEqualTo(String value) {
            addCriterion("topicimageUrl =", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlNotEqualTo(String value) {
            addCriterion("topicimageUrl <>", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlGreaterThan(String value) {
            addCriterion("topicimageUrl >", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlGreaterThanOrEqualTo(String value) {
            addCriterion("topicimageUrl >=", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlLessThan(String value) {
            addCriterion("topicimageUrl <", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlLessThanOrEqualTo(String value) {
            addCriterion("topicimageUrl <=", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlLike(String value) {
            addCriterion("topicimageUrl like", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlNotLike(String value) {
            addCriterion("topicimageUrl not like", value, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlIn(List<String> values) {
            addCriterion("topicimageUrl in", values, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlNotIn(List<String> values) {
            addCriterion("topicimageUrl not in", values, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlBetween(String value1, String value2) {
            addCriterion("topicimageUrl between", value1, value2, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageurlNotBetween(String value1, String value2) {
            addCriterion("topicimageUrl not between", value1, value2, "topicimageurl");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusIsNull() {
            addCriterion("topicimageAuditstatus is null");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusIsNotNull() {
            addCriterion("topicimageAuditstatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusEqualTo(Integer value) {
            addCriterion("topicimageAuditstatus =", value, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusNotEqualTo(Integer value) {
            addCriterion("topicimageAuditstatus <>", value, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusGreaterThan(Integer value) {
            addCriterion("topicimageAuditstatus >", value, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicimageAuditstatus >=", value, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusLessThan(Integer value) {
            addCriterion("topicimageAuditstatus <", value, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicimageAuditstatus <=", value, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusIn(List<Integer> values) {
            addCriterion("topicimageAuditstatus in", values, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusNotIn(List<Integer> values) {
            addCriterion("topicimageAuditstatus not in", values, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusBetween(Integer value1, Integer value2) {
            addCriterion("topicimageAuditstatus between", value1, value2, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimageauditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicimageAuditstatus not between", value1, value2, "topicimageauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusIsNull() {
            addCriterion("topicimageDelstatus is null");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusIsNotNull() {
            addCriterion("topicimageDelstatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusEqualTo(Integer value) {
            addCriterion("topicimageDelstatus =", value, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusNotEqualTo(Integer value) {
            addCriterion("topicimageDelstatus <>", value, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusGreaterThan(Integer value) {
            addCriterion("topicimageDelstatus >", value, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicimageDelstatus >=", value, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusLessThan(Integer value) {
            addCriterion("topicimageDelstatus <", value, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicimageDelstatus <=", value, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusIn(List<Integer> values) {
            addCriterion("topicimageDelstatus in", values, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusNotIn(List<Integer> values) {
            addCriterion("topicimageDelstatus not in", values, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusBetween(Integer value1, Integer value2) {
            addCriterion("topicimageDelstatus between", value1, value2, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagedelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicimageDelstatus not between", value1, value2, "topicimagedelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeIsNull() {
            addCriterion("topicimageCreatetime is null");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeIsNotNull() {
            addCriterion("topicimageCreatetime is not null");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeEqualTo(String value) {
            addCriterion("topicimageCreatetime =", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeNotEqualTo(String value) {
            addCriterion("topicimageCreatetime <>", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeGreaterThan(String value) {
            addCriterion("topicimageCreatetime >", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("topicimageCreatetime >=", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeLessThan(String value) {
            addCriterion("topicimageCreatetime <", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeLessThanOrEqualTo(String value) {
            addCriterion("topicimageCreatetime <=", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeLike(String value) {
            addCriterion("topicimageCreatetime like", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeNotLike(String value) {
            addCriterion("topicimageCreatetime not like", value, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeIn(List<String> values) {
            addCriterion("topicimageCreatetime in", values, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeNotIn(List<String> values) {
            addCriterion("topicimageCreatetime not in", values, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeBetween(String value1, String value2) {
            addCriterion("topicimageCreatetime between", value1, value2, "topicimagecreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicimagecreatetimeNotBetween(String value1, String value2) {
            addCriterion("topicimageCreatetime not between", value1, value2, "topicimagecreatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}