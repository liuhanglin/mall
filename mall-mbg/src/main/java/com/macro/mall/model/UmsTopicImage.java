package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsTopicImage implements Serializable {
    private Integer topicimageid;

    private Integer topicid;

    private String topicimageurl;

    private Integer topicimageauditstatus;

    private Integer topicimagedelstatus;

    private String topicimagecreatetime;

    private static final long serialVersionUID = 1L;

    public Integer getTopicimageid() {
        return topicimageid;
    }

    public void setTopicimageid(Integer topicimageid) {
        this.topicimageid = topicimageid;
    }

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public String getTopicimageurl() {
        return topicimageurl;
    }

    public void setTopicimageurl(String topicimageurl) {
        this.topicimageurl = topicimageurl;
    }

    public Integer getTopicimageauditstatus() {
        return topicimageauditstatus;
    }

    public void setTopicimageauditstatus(Integer topicimageauditstatus) {
        this.topicimageauditstatus = topicimageauditstatus;
    }

    public Integer getTopicimagedelstatus() {
        return topicimagedelstatus;
    }

    public void setTopicimagedelstatus(Integer topicimagedelstatus) {
        this.topicimagedelstatus = topicimagedelstatus;
    }

    public String getTopicimagecreatetime() {
        return topicimagecreatetime;
    }

    public void setTopicimagecreatetime(String topicimagecreatetime) {
        this.topicimagecreatetime = topicimagecreatetime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", topicimageid=").append(topicimageid);
        sb.append(", topicid=").append(topicid);
        sb.append(", topicimageurl=").append(topicimageurl);
        sb.append(", topicimageauditstatus=").append(topicimageauditstatus);
        sb.append(", topicimagedelstatus=").append(topicimagedelstatus);
        sb.append(", topicimagecreatetime=").append(topicimagecreatetime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}