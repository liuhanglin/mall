package com.macro.mall.model;

import java.util.ArrayList;
import java.util.List;

public class UmsUserCollectExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UmsUserCollectExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUsercollectidIsNull() {
            addCriterion("usercollectId is null");
            return (Criteria) this;
        }

        public Criteria andUsercollectidIsNotNull() {
            addCriterion("usercollectId is not null");
            return (Criteria) this;
        }

        public Criteria andUsercollectidEqualTo(Integer value) {
            addCriterion("usercollectId =", value, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidNotEqualTo(Integer value) {
            addCriterion("usercollectId <>", value, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidGreaterThan(Integer value) {
            addCriterion("usercollectId >", value, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidGreaterThanOrEqualTo(Integer value) {
            addCriterion("usercollectId >=", value, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidLessThan(Integer value) {
            addCriterion("usercollectId <", value, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidLessThanOrEqualTo(Integer value) {
            addCriterion("usercollectId <=", value, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidIn(List<Integer> values) {
            addCriterion("usercollectId in", values, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidNotIn(List<Integer> values) {
            addCriterion("usercollectId not in", values, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidBetween(Integer value1, Integer value2) {
            addCriterion("usercollectId between", value1, value2, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUsercollectidNotBetween(Integer value1, Integer value2) {
            addCriterion("usercollectId not between", value1, value2, "usercollectid");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNull() {
            addCriterion("topicId is null");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNotNull() {
            addCriterion("topicId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicidEqualTo(Integer value) {
            addCriterion("topicId =", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotEqualTo(Integer value) {
            addCriterion("topicId <>", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThan(Integer value) {
            addCriterion("topicId >", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicId >=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThan(Integer value) {
            addCriterion("topicId <", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThanOrEqualTo(Integer value) {
            addCriterion("topicId <=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidIn(List<Integer> values) {
            addCriterion("topicId in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotIn(List<Integer> values) {
            addCriterion("topicId not in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidBetween(Integer value1, Integer value2) {
            addCriterion("topicId between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicId not between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusIsNull() {
            addCriterion("usercollectDelstatus is null");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusIsNotNull() {
            addCriterion("usercollectDelstatus is not null");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusEqualTo(Integer value) {
            addCriterion("usercollectDelstatus =", value, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusNotEqualTo(Integer value) {
            addCriterion("usercollectDelstatus <>", value, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusGreaterThan(Integer value) {
            addCriterion("usercollectDelstatus >", value, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("usercollectDelstatus >=", value, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusLessThan(Integer value) {
            addCriterion("usercollectDelstatus <", value, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("usercollectDelstatus <=", value, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusIn(List<Integer> values) {
            addCriterion("usercollectDelstatus in", values, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusNotIn(List<Integer> values) {
            addCriterion("usercollectDelstatus not in", values, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusBetween(Integer value1, Integer value2) {
            addCriterion("usercollectDelstatus between", value1, value2, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectdelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("usercollectDelstatus not between", value1, value2, "usercollectdelstatus");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeIsNull() {
            addCriterion("usercollectCreatetime is null");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeIsNotNull() {
            addCriterion("usercollectCreatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeEqualTo(String value) {
            addCriterion("usercollectCreatetime =", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeNotEqualTo(String value) {
            addCriterion("usercollectCreatetime <>", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeGreaterThan(String value) {
            addCriterion("usercollectCreatetime >", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("usercollectCreatetime >=", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeLessThan(String value) {
            addCriterion("usercollectCreatetime <", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeLessThanOrEqualTo(String value) {
            addCriterion("usercollectCreatetime <=", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeLike(String value) {
            addCriterion("usercollectCreatetime like", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeNotLike(String value) {
            addCriterion("usercollectCreatetime not like", value, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeIn(List<String> values) {
            addCriterion("usercollectCreatetime in", values, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeNotIn(List<String> values) {
            addCriterion("usercollectCreatetime not in", values, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeBetween(String value1, String value2) {
            addCriterion("usercollectCreatetime between", value1, value2, "usercollectcreatetime");
            return (Criteria) this;
        }

        public Criteria andUsercollectcreatetimeNotBetween(String value1, String value2) {
            addCriterion("usercollectCreatetime not between", value1, value2, "usercollectcreatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}