package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsUserCollect implements Serializable {
    private Integer usercollectid;

    private String userid;

    private Integer topicid;

    private Integer usercollectdelstatus;

    private String usercollectcreatetime;

    private static final long serialVersionUID = 1L;

    public Integer getUsercollectid() {
        return usercollectid;
    }

    public void setUsercollectid(Integer usercollectid) {
        this.usercollectid = usercollectid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public Integer getUsercollectdelstatus() {
        return usercollectdelstatus;
    }

    public void setUsercollectdelstatus(Integer usercollectdelstatus) {
        this.usercollectdelstatus = usercollectdelstatus;
    }

    public String getUsercollectcreatetime() {
        return usercollectcreatetime;
    }

    public void setUsercollectcreatetime(String usercollectcreatetime) {
        this.usercollectcreatetime = usercollectcreatetime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", usercollectid=").append(usercollectid);
        sb.append(", userid=").append(userid);
        sb.append(", topicid=").append(topicid);
        sb.append(", usercollectdelstatus=").append(usercollectdelstatus);
        sb.append(", usercollectcreatetime=").append(usercollectcreatetime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}