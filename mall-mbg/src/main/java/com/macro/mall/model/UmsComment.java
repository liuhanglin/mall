package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsComment implements Serializable {
    private Integer commentid;

    private Integer topicid;

    private String userid;

    private String commentcontent;

    private Integer commentpraisecount;

    private String commentcreatetime;

    private Integer commenttopstatus;

    private Integer commentauditstatus;

    private Integer commentdelstatus;

    private Integer parentcommentid;

    private static final long serialVersionUID = 1L;

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCommentcontent() {
        return commentcontent;
    }

    public void setCommentcontent(String commentcontent) {
        this.commentcontent = commentcontent;
    }

    public Integer getCommentpraisecount() {
        return commentpraisecount;
    }

    public void setCommentpraisecount(Integer commentpraisecount) {
        this.commentpraisecount = commentpraisecount;
    }

    public String getCommentcreatetime() {
        return commentcreatetime;
    }

    public void setCommentcreatetime(String commentcreatetime) {
        this.commentcreatetime = commentcreatetime;
    }

    public Integer getCommenttopstatus() {
        return commenttopstatus;
    }

    public void setCommenttopstatus(Integer commenttopstatus) {
        this.commenttopstatus = commenttopstatus;
    }

    public Integer getCommentauditstatus() {
        return commentauditstatus;
    }

    public void setCommentauditstatus(Integer commentauditstatus) {
        this.commentauditstatus = commentauditstatus;
    }

    public Integer getCommentdelstatus() {
        return commentdelstatus;
    }

    public void setCommentdelstatus(Integer commentdelstatus) {
        this.commentdelstatus = commentdelstatus;
    }

    public Integer getParentcommentid() {
        return parentcommentid;
    }

    public void setParentcommentid(Integer parentcommentid) {
        this.parentcommentid = parentcommentid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", commentid=").append(commentid);
        sb.append(", topicid=").append(topicid);
        sb.append(", userid=").append(userid);
        sb.append(", commentcontent=").append(commentcontent);
        sb.append(", commentpraisecount=").append(commentpraisecount);
        sb.append(", commentcreatetime=").append(commentcreatetime);
        sb.append(", commenttopstatus=").append(commenttopstatus);
        sb.append(", commentauditstatus=").append(commentauditstatus);
        sb.append(", commentdelstatus=").append(commentdelstatus);
        sb.append(", parentcommentid=").append(parentcommentid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}