package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsTopic implements Serializable {
    private Integer topicid;

    private String userid;

    private String topictitle;

    private Integer topicbrowsecount;

    private String topiccreatetime;

    private Integer topicpublishstatus;

    private Integer topicauditstatus;

    private Integer topicdelstatus;

    private Integer topictopstatus;

    private Integer topicgendertype;

    private Integer topicheight;

    private Integer topicweight;

    private Integer topiceducation;

    private Integer topicmarried;

    private Integer topicfacescore;

    private String topictags;

    private String topicareacode;

    private String topicmodifytime;

    private Integer topicage;

    private String topiccover;

    private Integer topictemplatestatus;

    private Integer topicpraisecount;

    private String topiccontent;

    private String topicselfintroduction;

    private String topicdemand;

    private String topicinterest;

    private static final long serialVersionUID = 1L;

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTopictitle() {
        return topictitle;
    }

    public void setTopictitle(String topictitle) {
        this.topictitle = topictitle;
    }

    public Integer getTopicbrowsecount() {
        return topicbrowsecount;
    }

    public void setTopicbrowsecount(Integer topicbrowsecount) {
        this.topicbrowsecount = topicbrowsecount;
    }

    public String getTopiccreatetime() {
        return topiccreatetime;
    }

    public void setTopiccreatetime(String topiccreatetime) {
        this.topiccreatetime = topiccreatetime;
    }

    public Integer getTopicpublishstatus() {
        return topicpublishstatus;
    }

    public void setTopicpublishstatus(Integer topicpublishstatus) {
        this.topicpublishstatus = topicpublishstatus;
    }

    public Integer getTopicauditstatus() {
        return topicauditstatus;
    }

    public void setTopicauditstatus(Integer topicauditstatus) {
        this.topicauditstatus = topicauditstatus;
    }

    public Integer getTopicdelstatus() {
        return topicdelstatus;
    }

    public void setTopicdelstatus(Integer topicdelstatus) {
        this.topicdelstatus = topicdelstatus;
    }

    public Integer getTopictopstatus() {
        return topictopstatus;
    }

    public void setTopictopstatus(Integer topictopstatus) {
        this.topictopstatus = topictopstatus;
    }

    public Integer getTopicgendertype() {
        return topicgendertype;
    }

    public void setTopicgendertype(Integer topicgendertype) {
        this.topicgendertype = topicgendertype;
    }

    public Integer getTopicheight() {
        return topicheight;
    }

    public void setTopicheight(Integer topicheight) {
        this.topicheight = topicheight;
    }

    public Integer getTopicweight() {
        return topicweight;
    }

    public void setTopicweight(Integer topicweight) {
        this.topicweight = topicweight;
    }

    public Integer getTopiceducation() {
        return topiceducation;
    }

    public void setTopiceducation(Integer topiceducation) {
        this.topiceducation = topiceducation;
    }

    public Integer getTopicmarried() {
        return topicmarried;
    }

    public void setTopicmarried(Integer topicmarried) {
        this.topicmarried = topicmarried;
    }

    public Integer getTopicfacescore() {
        return topicfacescore;
    }

    public void setTopicfacescore(Integer topicfacescore) {
        this.topicfacescore = topicfacescore;
    }

    public String getTopictags() {
        return topictags;
    }

    public void setTopictags(String topictags) {
        this.topictags = topictags;
    }

    public String getTopicareacode() {
        return topicareacode;
    }

    public void setTopicareacode(String topicareacode) {
        this.topicareacode = topicareacode;
    }

    public String getTopicmodifytime() {
        return topicmodifytime;
    }

    public void setTopicmodifytime(String topicmodifytime) {
        this.topicmodifytime = topicmodifytime;
    }

    public Integer getTopicage() {
        return topicage;
    }

    public void setTopicage(Integer topicage) {
        this.topicage = topicage;
    }

    public String getTopiccover() {
        return topiccover;
    }

    public void setTopiccover(String topiccover) {
        this.topiccover = topiccover;
    }

    public Integer getTopictemplatestatus() {
        return topictemplatestatus;
    }

    public void setTopictemplatestatus(Integer topictemplatestatus) {
        this.topictemplatestatus = topictemplatestatus;
    }

    public Integer getTopicpraisecount() {
        return topicpraisecount;
    }

    public void setTopicpraisecount(Integer topicpraisecount) {
        this.topicpraisecount = topicpraisecount;
    }

    public String getTopiccontent() {
        return topiccontent;
    }

    public void setTopiccontent(String topiccontent) {
        this.topiccontent = topiccontent;
    }

    public String getTopicselfintroduction() {
        return topicselfintroduction;
    }

    public void setTopicselfintroduction(String topicselfintroduction) {
        this.topicselfintroduction = topicselfintroduction;
    }

    public String getTopicdemand() {
        return topicdemand;
    }

    public void setTopicdemand(String topicdemand) {
        this.topicdemand = topicdemand;
    }

    public String getTopicinterest() {
        return topicinterest;
    }

    public void setTopicinterest(String topicinterest) {
        this.topicinterest = topicinterest;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", topicid=").append(topicid);
        sb.append(", userid=").append(userid);
        sb.append(", topictitle=").append(topictitle);
        sb.append(", topicbrowsecount=").append(topicbrowsecount);
        sb.append(", topiccreatetime=").append(topiccreatetime);
        sb.append(", topicpublishstatus=").append(topicpublishstatus);
        sb.append(", topicauditstatus=").append(topicauditstatus);
        sb.append(", topicdelstatus=").append(topicdelstatus);
        sb.append(", topictopstatus=").append(topictopstatus);
        sb.append(", topicgendertype=").append(topicgendertype);
        sb.append(", topicheight=").append(topicheight);
        sb.append(", topicweight=").append(topicweight);
        sb.append(", topiceducation=").append(topiceducation);
        sb.append(", topicmarried=").append(topicmarried);
        sb.append(", topicfacescore=").append(topicfacescore);
        sb.append(", topictags=").append(topictags);
        sb.append(", topicareacode=").append(topicareacode);
        sb.append(", topicmodifytime=").append(topicmodifytime);
        sb.append(", topicage=").append(topicage);
        sb.append(", topiccover=").append(topiccover);
        sb.append(", topictemplatestatus=").append(topictemplatestatus);
        sb.append(", topicpraisecount=").append(topicpraisecount);
        sb.append(", topiccontent=").append(topiccontent);
        sb.append(", topicselfintroduction=").append(topicselfintroduction);
        sb.append(", topicdemand=").append(topicdemand);
        sb.append(", topicinterest=").append(topicinterest);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}