package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsUser implements Serializable {
    private Integer id;

    private String userid;

    private String openid;

    private String memberid;

    private String unnionid;

    private Integer type;

    private Integer status;

    private String createtime;

    private String birthday;

    private Integer height;

    private Integer weight;

    private String province;

    private String city;

    private String profession;

    private String annualsalary;

    private String wechatno;

    private Integer auditstatus;

    private String introduction;

    private String interests;

    private String demand;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getUnnionid() {
        return unnionid;
    }

    public void setUnnionid(String unnionid) {
        this.unnionid = unnionid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getAnnualsalary() {
        return annualsalary;
    }

    public void setAnnualsalary(String annualsalary) {
        this.annualsalary = annualsalary;
    }

    public String getWechatno() {
        return wechatno;
    }

    public void setWechatno(String wechatno) {
        this.wechatno = wechatno;
    }

    public Integer getAuditstatus() {
        return auditstatus;
    }

    public void setAuditstatus(Integer auditstatus) {
        this.auditstatus = auditstatus;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userid=").append(userid);
        sb.append(", openid=").append(openid);
        sb.append(", memberid=").append(memberid);
        sb.append(", unnionid=").append(unnionid);
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append(", createtime=").append(createtime);
        sb.append(", birthday=").append(birthday);
        sb.append(", height=").append(height);
        sb.append(", weight=").append(weight);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", profession=").append(profession);
        sb.append(", annualsalary=").append(annualsalary);
        sb.append(", wechatno=").append(wechatno);
        sb.append(", auditstatus=").append(auditstatus);
        sb.append(", introduction=").append(introduction);
        sb.append(", interests=").append(interests);
        sb.append(", demand=").append(demand);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}