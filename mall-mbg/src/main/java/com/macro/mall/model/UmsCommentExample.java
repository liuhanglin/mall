package com.macro.mall.model;

import java.util.ArrayList;
import java.util.List;

public class UmsCommentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UmsCommentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCommentidIsNull() {
            addCriterion("commentId is null");
            return (Criteria) this;
        }

        public Criteria andCommentidIsNotNull() {
            addCriterion("commentId is not null");
            return (Criteria) this;
        }

        public Criteria andCommentidEqualTo(Integer value) {
            addCriterion("commentId =", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotEqualTo(Integer value) {
            addCriterion("commentId <>", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidGreaterThan(Integer value) {
            addCriterion("commentId >", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentId >=", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidLessThan(Integer value) {
            addCriterion("commentId <", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidLessThanOrEqualTo(Integer value) {
            addCriterion("commentId <=", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidIn(List<Integer> values) {
            addCriterion("commentId in", values, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotIn(List<Integer> values) {
            addCriterion("commentId not in", values, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidBetween(Integer value1, Integer value2) {
            addCriterion("commentId between", value1, value2, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotBetween(Integer value1, Integer value2) {
            addCriterion("commentId not between", value1, value2, "commentid");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNull() {
            addCriterion("topicId is null");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNotNull() {
            addCriterion("topicId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicidEqualTo(Integer value) {
            addCriterion("topicId =", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotEqualTo(Integer value) {
            addCriterion("topicId <>", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThan(Integer value) {
            addCriterion("topicId >", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicId >=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThan(Integer value) {
            addCriterion("topicId <", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThanOrEqualTo(Integer value) {
            addCriterion("topicId <=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidIn(List<Integer> values) {
            addCriterion("topicId in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotIn(List<Integer> values) {
            addCriterion("topicId not in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidBetween(Integer value1, Integer value2) {
            addCriterion("topicId between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicId not between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andCommentcontentIsNull() {
            addCriterion("commentContent is null");
            return (Criteria) this;
        }

        public Criteria andCommentcontentIsNotNull() {
            addCriterion("commentContent is not null");
            return (Criteria) this;
        }

        public Criteria andCommentcontentEqualTo(String value) {
            addCriterion("commentContent =", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentNotEqualTo(String value) {
            addCriterion("commentContent <>", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentGreaterThan(String value) {
            addCriterion("commentContent >", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentGreaterThanOrEqualTo(String value) {
            addCriterion("commentContent >=", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentLessThan(String value) {
            addCriterion("commentContent <", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentLessThanOrEqualTo(String value) {
            addCriterion("commentContent <=", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentLike(String value) {
            addCriterion("commentContent like", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentNotLike(String value) {
            addCriterion("commentContent not like", value, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentIn(List<String> values) {
            addCriterion("commentContent in", values, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentNotIn(List<String> values) {
            addCriterion("commentContent not in", values, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentBetween(String value1, String value2) {
            addCriterion("commentContent between", value1, value2, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentcontentNotBetween(String value1, String value2) {
            addCriterion("commentContent not between", value1, value2, "commentcontent");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountIsNull() {
            addCriterion("commentPraisecount is null");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountIsNotNull() {
            addCriterion("commentPraisecount is not null");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountEqualTo(Integer value) {
            addCriterion("commentPraisecount =", value, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountNotEqualTo(Integer value) {
            addCriterion("commentPraisecount <>", value, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountGreaterThan(Integer value) {
            addCriterion("commentPraisecount >", value, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentPraisecount >=", value, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountLessThan(Integer value) {
            addCriterion("commentPraisecount <", value, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountLessThanOrEqualTo(Integer value) {
            addCriterion("commentPraisecount <=", value, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountIn(List<Integer> values) {
            addCriterion("commentPraisecount in", values, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountNotIn(List<Integer> values) {
            addCriterion("commentPraisecount not in", values, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountBetween(Integer value1, Integer value2) {
            addCriterion("commentPraisecount between", value1, value2, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentpraisecountNotBetween(Integer value1, Integer value2) {
            addCriterion("commentPraisecount not between", value1, value2, "commentpraisecount");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeIsNull() {
            addCriterion("commentCreatetime is null");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeIsNotNull() {
            addCriterion("commentCreatetime is not null");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeEqualTo(String value) {
            addCriterion("commentCreatetime =", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeNotEqualTo(String value) {
            addCriterion("commentCreatetime <>", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeGreaterThan(String value) {
            addCriterion("commentCreatetime >", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("commentCreatetime >=", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeLessThan(String value) {
            addCriterion("commentCreatetime <", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeLessThanOrEqualTo(String value) {
            addCriterion("commentCreatetime <=", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeLike(String value) {
            addCriterion("commentCreatetime like", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeNotLike(String value) {
            addCriterion("commentCreatetime not like", value, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeIn(List<String> values) {
            addCriterion("commentCreatetime in", values, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeNotIn(List<String> values) {
            addCriterion("commentCreatetime not in", values, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeBetween(String value1, String value2) {
            addCriterion("commentCreatetime between", value1, value2, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommentcreatetimeNotBetween(String value1, String value2) {
            addCriterion("commentCreatetime not between", value1, value2, "commentcreatetime");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusIsNull() {
            addCriterion("commentTopstatus is null");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusIsNotNull() {
            addCriterion("commentTopstatus is not null");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusEqualTo(Integer value) {
            addCriterion("commentTopstatus =", value, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusNotEqualTo(Integer value) {
            addCriterion("commentTopstatus <>", value, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusGreaterThan(Integer value) {
            addCriterion("commentTopstatus >", value, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentTopstatus >=", value, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusLessThan(Integer value) {
            addCriterion("commentTopstatus <", value, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusLessThanOrEqualTo(Integer value) {
            addCriterion("commentTopstatus <=", value, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusIn(List<Integer> values) {
            addCriterion("commentTopstatus in", values, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusNotIn(List<Integer> values) {
            addCriterion("commentTopstatus not in", values, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusBetween(Integer value1, Integer value2) {
            addCriterion("commentTopstatus between", value1, value2, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommenttopstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("commentTopstatus not between", value1, value2, "commenttopstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusIsNull() {
            addCriterion("commentAuditstatus is null");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusIsNotNull() {
            addCriterion("commentAuditstatus is not null");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusEqualTo(Integer value) {
            addCriterion("commentAuditstatus =", value, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusNotEqualTo(Integer value) {
            addCriterion("commentAuditstatus <>", value, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusGreaterThan(Integer value) {
            addCriterion("commentAuditstatus >", value, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentAuditstatus >=", value, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusLessThan(Integer value) {
            addCriterion("commentAuditstatus <", value, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("commentAuditstatus <=", value, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusIn(List<Integer> values) {
            addCriterion("commentAuditstatus in", values, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusNotIn(List<Integer> values) {
            addCriterion("commentAuditstatus not in", values, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusBetween(Integer value1, Integer value2) {
            addCriterion("commentAuditstatus between", value1, value2, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentauditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("commentAuditstatus not between", value1, value2, "commentauditstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusIsNull() {
            addCriterion("commentDelstatus is null");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusIsNotNull() {
            addCriterion("commentDelstatus is not null");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusEqualTo(Integer value) {
            addCriterion("commentDelstatus =", value, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusNotEqualTo(Integer value) {
            addCriterion("commentDelstatus <>", value, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusGreaterThan(Integer value) {
            addCriterion("commentDelstatus >", value, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentDelstatus >=", value, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusLessThan(Integer value) {
            addCriterion("commentDelstatus <", value, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("commentDelstatus <=", value, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusIn(List<Integer> values) {
            addCriterion("commentDelstatus in", values, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusNotIn(List<Integer> values) {
            addCriterion("commentDelstatus not in", values, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusBetween(Integer value1, Integer value2) {
            addCriterion("commentDelstatus between", value1, value2, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andCommentdelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("commentDelstatus not between", value1, value2, "commentdelstatus");
            return (Criteria) this;
        }

        public Criteria andParentcommentidIsNull() {
            addCriterion("parentCommentid is null");
            return (Criteria) this;
        }

        public Criteria andParentcommentidIsNotNull() {
            addCriterion("parentCommentid is not null");
            return (Criteria) this;
        }

        public Criteria andParentcommentidEqualTo(Integer value) {
            addCriterion("parentCommentid =", value, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidNotEqualTo(Integer value) {
            addCriterion("parentCommentid <>", value, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidGreaterThan(Integer value) {
            addCriterion("parentCommentid >", value, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("parentCommentid >=", value, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidLessThan(Integer value) {
            addCriterion("parentCommentid <", value, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidLessThanOrEqualTo(Integer value) {
            addCriterion("parentCommentid <=", value, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidIn(List<Integer> values) {
            addCriterion("parentCommentid in", values, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidNotIn(List<Integer> values) {
            addCriterion("parentCommentid not in", values, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidBetween(Integer value1, Integer value2) {
            addCriterion("parentCommentid between", value1, value2, "parentcommentid");
            return (Criteria) this;
        }

        public Criteria andParentcommentidNotBetween(Integer value1, Integer value2) {
            addCriterion("parentCommentid not between", value1, value2, "parentcommentid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}