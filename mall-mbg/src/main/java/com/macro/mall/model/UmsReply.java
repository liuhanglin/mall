package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsReply implements Serializable {
    private Integer replyid;

    private String userid;

    private Integer topicid;

    private Integer commentid;

    private Integer parentreplyid;

    private String parentuserid;

    private Integer replypraisecount;

    private String replycontent;

    private String replycreatetime;

    private Integer replyauditstatus;

    private Integer replydelstatus;

    private static final long serialVersionUID = 1L;

    public Integer getReplyid() {
        return replyid;
    }

    public void setReplyid(Integer replyid) {
        this.replyid = replyid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Integer getParentreplyid() {
        return parentreplyid;
    }

    public void setParentreplyid(Integer parentreplyid) {
        this.parentreplyid = parentreplyid;
    }

    public String getParentuserid() {
        return parentuserid;
    }

    public void setParentuserid(String parentuserid) {
        this.parentuserid = parentuserid;
    }

    public Integer getReplypraisecount() {
        return replypraisecount;
    }

    public void setReplypraisecount(Integer replypraisecount) {
        this.replypraisecount = replypraisecount;
    }

    public String getReplycontent() {
        return replycontent;
    }

    public void setReplycontent(String replycontent) {
        this.replycontent = replycontent;
    }

    public String getReplycreatetime() {
        return replycreatetime;
    }

    public void setReplycreatetime(String replycreatetime) {
        this.replycreatetime = replycreatetime;
    }

    public Integer getReplyauditstatus() {
        return replyauditstatus;
    }

    public void setReplyauditstatus(Integer replyauditstatus) {
        this.replyauditstatus = replyauditstatus;
    }

    public Integer getReplydelstatus() {
        return replydelstatus;
    }

    public void setReplydelstatus(Integer replydelstatus) {
        this.replydelstatus = replydelstatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", replyid=").append(replyid);
        sb.append(", userid=").append(userid);
        sb.append(", topicid=").append(topicid);
        sb.append(", commentid=").append(commentid);
        sb.append(", parentreplyid=").append(parentreplyid);
        sb.append(", parentuserid=").append(parentuserid);
        sb.append(", replypraisecount=").append(replypraisecount);
        sb.append(", replycontent=").append(replycontent);
        sb.append(", replycreatetime=").append(replycreatetime);
        sb.append(", replyauditstatus=").append(replyauditstatus);
        sb.append(", replydelstatus=").append(replydelstatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}