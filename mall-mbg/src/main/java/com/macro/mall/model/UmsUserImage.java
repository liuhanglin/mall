package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsUserImage implements Serializable {
    private Integer userimageid;

    private String userid;

    private String imageurl;

    private String createtime;

    private Integer auditstatus;

    private static final long serialVersionUID = 1L;

    public Integer getUserimageid() {
        return userimageid;
    }

    public void setUserimageid(Integer userimageid) {
        this.userimageid = userimageid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Integer getAuditstatus() {
        return auditstatus;
    }

    public void setAuditstatus(Integer auditstatus) {
        this.auditstatus = auditstatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userimageid=").append(userimageid);
        sb.append(", userid=").append(userid);
        sb.append(", imageurl=").append(imageurl);
        sb.append(", createtime=").append(createtime);
        sb.append(", auditstatus=").append(auditstatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}