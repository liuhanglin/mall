package com.macro.mall.model;

import java.util.ArrayList;
import java.util.List;

public class UmsTopicExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UmsTopicExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTopicidIsNull() {
            addCriterion("topicId is null");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNotNull() {
            addCriterion("topicId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicidEqualTo(Integer value) {
            addCriterion("topicId =", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotEqualTo(Integer value) {
            addCriterion("topicId <>", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThan(Integer value) {
            addCriterion("topicId >", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicId >=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThan(Integer value) {
            addCriterion("topicId <", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThanOrEqualTo(Integer value) {
            addCriterion("topicId <=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidIn(List<Integer> values) {
            addCriterion("topicId in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotIn(List<Integer> values) {
            addCriterion("topicId not in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidBetween(Integer value1, Integer value2) {
            addCriterion("topicId between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicId not between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andTopictitleIsNull() {
            addCriterion("topicTitle is null");
            return (Criteria) this;
        }

        public Criteria andTopictitleIsNotNull() {
            addCriterion("topicTitle is not null");
            return (Criteria) this;
        }

        public Criteria andTopictitleEqualTo(String value) {
            addCriterion("topicTitle =", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleNotEqualTo(String value) {
            addCriterion("topicTitle <>", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleGreaterThan(String value) {
            addCriterion("topicTitle >", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleGreaterThanOrEqualTo(String value) {
            addCriterion("topicTitle >=", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleLessThan(String value) {
            addCriterion("topicTitle <", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleLessThanOrEqualTo(String value) {
            addCriterion("topicTitle <=", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleLike(String value) {
            addCriterion("topicTitle like", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleNotLike(String value) {
            addCriterion("topicTitle not like", value, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleIn(List<String> values) {
            addCriterion("topicTitle in", values, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleNotIn(List<String> values) {
            addCriterion("topicTitle not in", values, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleBetween(String value1, String value2) {
            addCriterion("topicTitle between", value1, value2, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopictitleNotBetween(String value1, String value2) {
            addCriterion("topicTitle not between", value1, value2, "topictitle");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountIsNull() {
            addCriterion("topicBrowsecount is null");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountIsNotNull() {
            addCriterion("topicBrowsecount is not null");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountEqualTo(Integer value) {
            addCriterion("topicBrowsecount =", value, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountNotEqualTo(Integer value) {
            addCriterion("topicBrowsecount <>", value, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountGreaterThan(Integer value) {
            addCriterion("topicBrowsecount >", value, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicBrowsecount >=", value, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountLessThan(Integer value) {
            addCriterion("topicBrowsecount <", value, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountLessThanOrEqualTo(Integer value) {
            addCriterion("topicBrowsecount <=", value, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountIn(List<Integer> values) {
            addCriterion("topicBrowsecount in", values, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountNotIn(List<Integer> values) {
            addCriterion("topicBrowsecount not in", values, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountBetween(Integer value1, Integer value2) {
            addCriterion("topicBrowsecount between", value1, value2, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopicbrowsecountNotBetween(Integer value1, Integer value2) {
            addCriterion("topicBrowsecount not between", value1, value2, "topicbrowsecount");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeIsNull() {
            addCriterion("topicCreatetime is null");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeIsNotNull() {
            addCriterion("topicCreatetime is not null");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeEqualTo(String value) {
            addCriterion("topicCreatetime =", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeNotEqualTo(String value) {
            addCriterion("topicCreatetime <>", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeGreaterThan(String value) {
            addCriterion("topicCreatetime >", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("topicCreatetime >=", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeLessThan(String value) {
            addCriterion("topicCreatetime <", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeLessThanOrEqualTo(String value) {
            addCriterion("topicCreatetime <=", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeLike(String value) {
            addCriterion("topicCreatetime like", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeNotLike(String value) {
            addCriterion("topicCreatetime not like", value, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeIn(List<String> values) {
            addCriterion("topicCreatetime in", values, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeNotIn(List<String> values) {
            addCriterion("topicCreatetime not in", values, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeBetween(String value1, String value2) {
            addCriterion("topicCreatetime between", value1, value2, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopiccreatetimeNotBetween(String value1, String value2) {
            addCriterion("topicCreatetime not between", value1, value2, "topiccreatetime");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusIsNull() {
            addCriterion("topicPublishstatus is null");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusIsNotNull() {
            addCriterion("topicPublishstatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusEqualTo(Integer value) {
            addCriterion("topicPublishstatus =", value, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusNotEqualTo(Integer value) {
            addCriterion("topicPublishstatus <>", value, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusGreaterThan(Integer value) {
            addCriterion("topicPublishstatus >", value, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicPublishstatus >=", value, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusLessThan(Integer value) {
            addCriterion("topicPublishstatus <", value, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicPublishstatus <=", value, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusIn(List<Integer> values) {
            addCriterion("topicPublishstatus in", values, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusNotIn(List<Integer> values) {
            addCriterion("topicPublishstatus not in", values, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusBetween(Integer value1, Integer value2) {
            addCriterion("topicPublishstatus between", value1, value2, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicpublishstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicPublishstatus not between", value1, value2, "topicpublishstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusIsNull() {
            addCriterion("topicAuditstatus is null");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusIsNotNull() {
            addCriterion("topicAuditstatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusEqualTo(Integer value) {
            addCriterion("topicAuditstatus =", value, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusNotEqualTo(Integer value) {
            addCriterion("topicAuditstatus <>", value, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusGreaterThan(Integer value) {
            addCriterion("topicAuditstatus >", value, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicAuditstatus >=", value, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusLessThan(Integer value) {
            addCriterion("topicAuditstatus <", value, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicAuditstatus <=", value, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusIn(List<Integer> values) {
            addCriterion("topicAuditstatus in", values, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusNotIn(List<Integer> values) {
            addCriterion("topicAuditstatus not in", values, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusBetween(Integer value1, Integer value2) {
            addCriterion("topicAuditstatus between", value1, value2, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicauditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicAuditstatus not between", value1, value2, "topicauditstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusIsNull() {
            addCriterion("topicDelstatus is null");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusIsNotNull() {
            addCriterion("topicDelstatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusEqualTo(Integer value) {
            addCriterion("topicDelstatus =", value, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusNotEqualTo(Integer value) {
            addCriterion("topicDelstatus <>", value, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusGreaterThan(Integer value) {
            addCriterion("topicDelstatus >", value, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicDelstatus >=", value, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusLessThan(Integer value) {
            addCriterion("topicDelstatus <", value, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicDelstatus <=", value, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusIn(List<Integer> values) {
            addCriterion("topicDelstatus in", values, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusNotIn(List<Integer> values) {
            addCriterion("topicDelstatus not in", values, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusBetween(Integer value1, Integer value2) {
            addCriterion("topicDelstatus between", value1, value2, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopicdelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicDelstatus not between", value1, value2, "topicdelstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusIsNull() {
            addCriterion("topicTopstatus is null");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusIsNotNull() {
            addCriterion("topicTopstatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusEqualTo(Integer value) {
            addCriterion("topicTopstatus =", value, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusNotEqualTo(Integer value) {
            addCriterion("topicTopstatus <>", value, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusGreaterThan(Integer value) {
            addCriterion("topicTopstatus >", value, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicTopstatus >=", value, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusLessThan(Integer value) {
            addCriterion("topicTopstatus <", value, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicTopstatus <=", value, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusIn(List<Integer> values) {
            addCriterion("topicTopstatus in", values, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusNotIn(List<Integer> values) {
            addCriterion("topicTopstatus not in", values, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusBetween(Integer value1, Integer value2) {
            addCriterion("topicTopstatus between", value1, value2, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopictopstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicTopstatus not between", value1, value2, "topictopstatus");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeIsNull() {
            addCriterion("topicGendertype is null");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeIsNotNull() {
            addCriterion("topicGendertype is not null");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeEqualTo(Integer value) {
            addCriterion("topicGendertype =", value, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeNotEqualTo(Integer value) {
            addCriterion("topicGendertype <>", value, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeGreaterThan(Integer value) {
            addCriterion("topicGendertype >", value, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicGendertype >=", value, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeLessThan(Integer value) {
            addCriterion("topicGendertype <", value, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeLessThanOrEqualTo(Integer value) {
            addCriterion("topicGendertype <=", value, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeIn(List<Integer> values) {
            addCriterion("topicGendertype in", values, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeNotIn(List<Integer> values) {
            addCriterion("topicGendertype not in", values, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeBetween(Integer value1, Integer value2) {
            addCriterion("topicGendertype between", value1, value2, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicgendertypeNotBetween(Integer value1, Integer value2) {
            addCriterion("topicGendertype not between", value1, value2, "topicgendertype");
            return (Criteria) this;
        }

        public Criteria andTopicheightIsNull() {
            addCriterion("topicHeight is null");
            return (Criteria) this;
        }

        public Criteria andTopicheightIsNotNull() {
            addCriterion("topicHeight is not null");
            return (Criteria) this;
        }

        public Criteria andTopicheightEqualTo(Integer value) {
            addCriterion("topicHeight =", value, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightNotEqualTo(Integer value) {
            addCriterion("topicHeight <>", value, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightGreaterThan(Integer value) {
            addCriterion("topicHeight >", value, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicHeight >=", value, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightLessThan(Integer value) {
            addCriterion("topicHeight <", value, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightLessThanOrEqualTo(Integer value) {
            addCriterion("topicHeight <=", value, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightIn(List<Integer> values) {
            addCriterion("topicHeight in", values, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightNotIn(List<Integer> values) {
            addCriterion("topicHeight not in", values, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightBetween(Integer value1, Integer value2) {
            addCriterion("topicHeight between", value1, value2, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicheightNotBetween(Integer value1, Integer value2) {
            addCriterion("topicHeight not between", value1, value2, "topicheight");
            return (Criteria) this;
        }

        public Criteria andTopicweightIsNull() {
            addCriterion("topicWeight is null");
            return (Criteria) this;
        }

        public Criteria andTopicweightIsNotNull() {
            addCriterion("topicWeight is not null");
            return (Criteria) this;
        }

        public Criteria andTopicweightEqualTo(Integer value) {
            addCriterion("topicWeight =", value, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightNotEqualTo(Integer value) {
            addCriterion("topicWeight <>", value, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightGreaterThan(Integer value) {
            addCriterion("topicWeight >", value, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicWeight >=", value, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightLessThan(Integer value) {
            addCriterion("topicWeight <", value, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightLessThanOrEqualTo(Integer value) {
            addCriterion("topicWeight <=", value, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightIn(List<Integer> values) {
            addCriterion("topicWeight in", values, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightNotIn(List<Integer> values) {
            addCriterion("topicWeight not in", values, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightBetween(Integer value1, Integer value2) {
            addCriterion("topicWeight between", value1, value2, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopicweightNotBetween(Integer value1, Integer value2) {
            addCriterion("topicWeight not between", value1, value2, "topicweight");
            return (Criteria) this;
        }

        public Criteria andTopiceducationIsNull() {
            addCriterion("topicEducation is null");
            return (Criteria) this;
        }

        public Criteria andTopiceducationIsNotNull() {
            addCriterion("topicEducation is not null");
            return (Criteria) this;
        }

        public Criteria andTopiceducationEqualTo(Integer value) {
            addCriterion("topicEducation =", value, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationNotEqualTo(Integer value) {
            addCriterion("topicEducation <>", value, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationGreaterThan(Integer value) {
            addCriterion("topicEducation >", value, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicEducation >=", value, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationLessThan(Integer value) {
            addCriterion("topicEducation <", value, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationLessThanOrEqualTo(Integer value) {
            addCriterion("topicEducation <=", value, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationIn(List<Integer> values) {
            addCriterion("topicEducation in", values, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationNotIn(List<Integer> values) {
            addCriterion("topicEducation not in", values, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationBetween(Integer value1, Integer value2) {
            addCriterion("topicEducation between", value1, value2, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopiceducationNotBetween(Integer value1, Integer value2) {
            addCriterion("topicEducation not between", value1, value2, "topiceducation");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedIsNull() {
            addCriterion("topicMarried is null");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedIsNotNull() {
            addCriterion("topicMarried is not null");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedEqualTo(Integer value) {
            addCriterion("topicMarried =", value, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedNotEqualTo(Integer value) {
            addCriterion("topicMarried <>", value, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedGreaterThan(Integer value) {
            addCriterion("topicMarried >", value, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicMarried >=", value, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedLessThan(Integer value) {
            addCriterion("topicMarried <", value, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedLessThanOrEqualTo(Integer value) {
            addCriterion("topicMarried <=", value, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedIn(List<Integer> values) {
            addCriterion("topicMarried in", values, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedNotIn(List<Integer> values) {
            addCriterion("topicMarried not in", values, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedBetween(Integer value1, Integer value2) {
            addCriterion("topicMarried between", value1, value2, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicmarriedNotBetween(Integer value1, Integer value2) {
            addCriterion("topicMarried not between", value1, value2, "topicmarried");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreIsNull() {
            addCriterion("topicFacescore is null");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreIsNotNull() {
            addCriterion("topicFacescore is not null");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreEqualTo(Integer value) {
            addCriterion("topicFacescore =", value, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreNotEqualTo(Integer value) {
            addCriterion("topicFacescore <>", value, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreGreaterThan(Integer value) {
            addCriterion("topicFacescore >", value, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicFacescore >=", value, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreLessThan(Integer value) {
            addCriterion("topicFacescore <", value, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreLessThanOrEqualTo(Integer value) {
            addCriterion("topicFacescore <=", value, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreIn(List<Integer> values) {
            addCriterion("topicFacescore in", values, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreNotIn(List<Integer> values) {
            addCriterion("topicFacescore not in", values, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreBetween(Integer value1, Integer value2) {
            addCriterion("topicFacescore between", value1, value2, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopicfacescoreNotBetween(Integer value1, Integer value2) {
            addCriterion("topicFacescore not between", value1, value2, "topicfacescore");
            return (Criteria) this;
        }

        public Criteria andTopictagsIsNull() {
            addCriterion("topicTags is null");
            return (Criteria) this;
        }

        public Criteria andTopictagsIsNotNull() {
            addCriterion("topicTags is not null");
            return (Criteria) this;
        }

        public Criteria andTopictagsEqualTo(String value) {
            addCriterion("topicTags =", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsNotEqualTo(String value) {
            addCriterion("topicTags <>", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsGreaterThan(String value) {
            addCriterion("topicTags >", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsGreaterThanOrEqualTo(String value) {
            addCriterion("topicTags >=", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsLessThan(String value) {
            addCriterion("topicTags <", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsLessThanOrEqualTo(String value) {
            addCriterion("topicTags <=", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsLike(String value) {
            addCriterion("topicTags like", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsNotLike(String value) {
            addCriterion("topicTags not like", value, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsIn(List<String> values) {
            addCriterion("topicTags in", values, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsNotIn(List<String> values) {
            addCriterion("topicTags not in", values, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsBetween(String value1, String value2) {
            addCriterion("topicTags between", value1, value2, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopictagsNotBetween(String value1, String value2) {
            addCriterion("topicTags not between", value1, value2, "topictags");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeIsNull() {
            addCriterion("topicAreacode is null");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeIsNotNull() {
            addCriterion("topicAreacode is not null");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeEqualTo(String value) {
            addCriterion("topicAreacode =", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeNotEqualTo(String value) {
            addCriterion("topicAreacode <>", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeGreaterThan(String value) {
            addCriterion("topicAreacode >", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeGreaterThanOrEqualTo(String value) {
            addCriterion("topicAreacode >=", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeLessThan(String value) {
            addCriterion("topicAreacode <", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeLessThanOrEqualTo(String value) {
            addCriterion("topicAreacode <=", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeLike(String value) {
            addCriterion("topicAreacode like", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeNotLike(String value) {
            addCriterion("topicAreacode not like", value, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeIn(List<String> values) {
            addCriterion("topicAreacode in", values, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeNotIn(List<String> values) {
            addCriterion("topicAreacode not in", values, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeBetween(String value1, String value2) {
            addCriterion("topicAreacode between", value1, value2, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicareacodeNotBetween(String value1, String value2) {
            addCriterion("topicAreacode not between", value1, value2, "topicareacode");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeIsNull() {
            addCriterion("topicModifytime is null");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeIsNotNull() {
            addCriterion("topicModifytime is not null");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeEqualTo(String value) {
            addCriterion("topicModifytime =", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeNotEqualTo(String value) {
            addCriterion("topicModifytime <>", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeGreaterThan(String value) {
            addCriterion("topicModifytime >", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeGreaterThanOrEqualTo(String value) {
            addCriterion("topicModifytime >=", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeLessThan(String value) {
            addCriterion("topicModifytime <", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeLessThanOrEqualTo(String value) {
            addCriterion("topicModifytime <=", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeLike(String value) {
            addCriterion("topicModifytime like", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeNotLike(String value) {
            addCriterion("topicModifytime not like", value, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeIn(List<String> values) {
            addCriterion("topicModifytime in", values, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeNotIn(List<String> values) {
            addCriterion("topicModifytime not in", values, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeBetween(String value1, String value2) {
            addCriterion("topicModifytime between", value1, value2, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicmodifytimeNotBetween(String value1, String value2) {
            addCriterion("topicModifytime not between", value1, value2, "topicmodifytime");
            return (Criteria) this;
        }

        public Criteria andTopicageIsNull() {
            addCriterion("topicAge is null");
            return (Criteria) this;
        }

        public Criteria andTopicageIsNotNull() {
            addCriterion("topicAge is not null");
            return (Criteria) this;
        }

        public Criteria andTopicageEqualTo(Integer value) {
            addCriterion("topicAge =", value, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageNotEqualTo(Integer value) {
            addCriterion("topicAge <>", value, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageGreaterThan(Integer value) {
            addCriterion("topicAge >", value, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicAge >=", value, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageLessThan(Integer value) {
            addCriterion("topicAge <", value, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageLessThanOrEqualTo(Integer value) {
            addCriterion("topicAge <=", value, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageIn(List<Integer> values) {
            addCriterion("topicAge in", values, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageNotIn(List<Integer> values) {
            addCriterion("topicAge not in", values, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageBetween(Integer value1, Integer value2) {
            addCriterion("topicAge between", value1, value2, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopicageNotBetween(Integer value1, Integer value2) {
            addCriterion("topicAge not between", value1, value2, "topicage");
            return (Criteria) this;
        }

        public Criteria andTopiccoverIsNull() {
            addCriterion("topicCover is null");
            return (Criteria) this;
        }

        public Criteria andTopiccoverIsNotNull() {
            addCriterion("topicCover is not null");
            return (Criteria) this;
        }

        public Criteria andTopiccoverEqualTo(String value) {
            addCriterion("topicCover =", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverNotEqualTo(String value) {
            addCriterion("topicCover <>", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverGreaterThan(String value) {
            addCriterion("topicCover >", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverGreaterThanOrEqualTo(String value) {
            addCriterion("topicCover >=", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverLessThan(String value) {
            addCriterion("topicCover <", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverLessThanOrEqualTo(String value) {
            addCriterion("topicCover <=", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverLike(String value) {
            addCriterion("topicCover like", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverNotLike(String value) {
            addCriterion("topicCover not like", value, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverIn(List<String> values) {
            addCriterion("topicCover in", values, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverNotIn(List<String> values) {
            addCriterion("topicCover not in", values, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverBetween(String value1, String value2) {
            addCriterion("topicCover between", value1, value2, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopiccoverNotBetween(String value1, String value2) {
            addCriterion("topicCover not between", value1, value2, "topiccover");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusIsNull() {
            addCriterion("topicTemplatestatus is null");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusIsNotNull() {
            addCriterion("topicTemplatestatus is not null");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusEqualTo(Integer value) {
            addCriterion("topicTemplatestatus =", value, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusNotEqualTo(Integer value) {
            addCriterion("topicTemplatestatus <>", value, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusGreaterThan(Integer value) {
            addCriterion("topicTemplatestatus >", value, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicTemplatestatus >=", value, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusLessThan(Integer value) {
            addCriterion("topicTemplatestatus <", value, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusLessThanOrEqualTo(Integer value) {
            addCriterion("topicTemplatestatus <=", value, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusIn(List<Integer> values) {
            addCriterion("topicTemplatestatus in", values, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusNotIn(List<Integer> values) {
            addCriterion("topicTemplatestatus not in", values, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusBetween(Integer value1, Integer value2) {
            addCriterion("topicTemplatestatus between", value1, value2, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopictemplatestatusNotBetween(Integer value1, Integer value2) {
            addCriterion("topicTemplatestatus not between", value1, value2, "topictemplatestatus");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountIsNull() {
            addCriterion("topicPraisecount is null");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountIsNotNull() {
            addCriterion("topicPraisecount is not null");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountEqualTo(Integer value) {
            addCriterion("topicPraisecount =", value, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountNotEqualTo(Integer value) {
            addCriterion("topicPraisecount <>", value, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountGreaterThan(Integer value) {
            addCriterion("topicPraisecount >", value, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicPraisecount >=", value, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountLessThan(Integer value) {
            addCriterion("topicPraisecount <", value, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountLessThanOrEqualTo(Integer value) {
            addCriterion("topicPraisecount <=", value, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountIn(List<Integer> values) {
            addCriterion("topicPraisecount in", values, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountNotIn(List<Integer> values) {
            addCriterion("topicPraisecount not in", values, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountBetween(Integer value1, Integer value2) {
            addCriterion("topicPraisecount between", value1, value2, "topicpraisecount");
            return (Criteria) this;
        }

        public Criteria andTopicpraisecountNotBetween(Integer value1, Integer value2) {
            addCriterion("topicPraisecount not between", value1, value2, "topicpraisecount");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}