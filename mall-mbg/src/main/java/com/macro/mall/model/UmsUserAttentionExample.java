package com.macro.mall.model;

import java.util.ArrayList;
import java.util.List;

public class UmsUserAttentionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UmsUserAttentionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserattentionidIsNull() {
            addCriterion("userattentionId is null");
            return (Criteria) this;
        }

        public Criteria andUserattentionidIsNotNull() {
            addCriterion("userattentionId is not null");
            return (Criteria) this;
        }

        public Criteria andUserattentionidEqualTo(Integer value) {
            addCriterion("userattentionId =", value, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidNotEqualTo(Integer value) {
            addCriterion("userattentionId <>", value, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidGreaterThan(Integer value) {
            addCriterion("userattentionId >", value, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidGreaterThanOrEqualTo(Integer value) {
            addCriterion("userattentionId >=", value, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidLessThan(Integer value) {
            addCriterion("userattentionId <", value, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidLessThanOrEqualTo(Integer value) {
            addCriterion("userattentionId <=", value, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidIn(List<Integer> values) {
            addCriterion("userattentionId in", values, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidNotIn(List<Integer> values) {
            addCriterion("userattentionId not in", values, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidBetween(Integer value1, Integer value2) {
            addCriterion("userattentionId between", value1, value2, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUserattentionidNotBetween(Integer value1, Integer value2) {
            addCriterion("userattentionId not between", value1, value2, "userattentionid");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andFuseridIsNull() {
            addCriterion("fuserId is null");
            return (Criteria) this;
        }

        public Criteria andFuseridIsNotNull() {
            addCriterion("fuserId is not null");
            return (Criteria) this;
        }

        public Criteria andFuseridEqualTo(String value) {
            addCriterion("fuserId =", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridNotEqualTo(String value) {
            addCriterion("fuserId <>", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridGreaterThan(String value) {
            addCriterion("fuserId >", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridGreaterThanOrEqualTo(String value) {
            addCriterion("fuserId >=", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridLessThan(String value) {
            addCriterion("fuserId <", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridLessThanOrEqualTo(String value) {
            addCriterion("fuserId <=", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridLike(String value) {
            addCriterion("fuserId like", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridNotLike(String value) {
            addCriterion("fuserId not like", value, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridIn(List<String> values) {
            addCriterion("fuserId in", values, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridNotIn(List<String> values) {
            addCriterion("fuserId not in", values, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridBetween(String value1, String value2) {
            addCriterion("fuserId between", value1, value2, "fuserid");
            return (Criteria) this;
        }

        public Criteria andFuseridNotBetween(String value1, String value2) {
            addCriterion("fuserId not between", value1, value2, "fuserid");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusIsNull() {
            addCriterion("userattentionDelstatus is null");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusIsNotNull() {
            addCriterion("userattentionDelstatus is not null");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusEqualTo(Integer value) {
            addCriterion("userattentionDelstatus =", value, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusNotEqualTo(Integer value) {
            addCriterion("userattentionDelstatus <>", value, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusGreaterThan(Integer value) {
            addCriterion("userattentionDelstatus >", value, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("userattentionDelstatus >=", value, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusLessThan(Integer value) {
            addCriterion("userattentionDelstatus <", value, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("userattentionDelstatus <=", value, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusIn(List<Integer> values) {
            addCriterion("userattentionDelstatus in", values, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusNotIn(List<Integer> values) {
            addCriterion("userattentionDelstatus not in", values, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusBetween(Integer value1, Integer value2) {
            addCriterion("userattentionDelstatus between", value1, value2, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentiondelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("userattentionDelstatus not between", value1, value2, "userattentiondelstatus");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeIsNull() {
            addCriterion("userattentionCreatetime is null");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeIsNotNull() {
            addCriterion("userattentionCreatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeEqualTo(String value) {
            addCriterion("userattentionCreatetime =", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeNotEqualTo(String value) {
            addCriterion("userattentionCreatetime <>", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeGreaterThan(String value) {
            addCriterion("userattentionCreatetime >", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("userattentionCreatetime >=", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeLessThan(String value) {
            addCriterion("userattentionCreatetime <", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeLessThanOrEqualTo(String value) {
            addCriterion("userattentionCreatetime <=", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeLike(String value) {
            addCriterion("userattentionCreatetime like", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeNotLike(String value) {
            addCriterion("userattentionCreatetime not like", value, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeIn(List<String> values) {
            addCriterion("userattentionCreatetime in", values, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeNotIn(List<String> values) {
            addCriterion("userattentionCreatetime not in", values, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeBetween(String value1, String value2) {
            addCriterion("userattentionCreatetime between", value1, value2, "userattentioncreatetime");
            return (Criteria) this;
        }

        public Criteria andUserattentioncreatetimeNotBetween(String value1, String value2) {
            addCriterion("userattentionCreatetime not between", value1, value2, "userattentioncreatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}