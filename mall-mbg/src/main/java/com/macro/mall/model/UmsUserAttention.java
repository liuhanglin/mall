package com.macro.mall.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class UmsUserAttention implements Serializable {
    private Integer userattentionid;

    private String userid;

    private String fuserid;

    private Integer userattentiondelstatus;

    private String userattentioncreatetime;

    private static final long serialVersionUID = 1L;

    public Integer getUserattentionid() {
        return userattentionid;
    }

    public void setUserattentionid(Integer userattentionid) {
        this.userattentionid = userattentionid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFuserid() {
        return fuserid;
    }

    public void setFuserid(String fuserid) {
        this.fuserid = fuserid;
    }

    public Integer getUserattentiondelstatus() {
        return userattentiondelstatus;
    }

    public void setUserattentiondelstatus(Integer userattentiondelstatus) {
        this.userattentiondelstatus = userattentiondelstatus;
    }

    public String getUserattentioncreatetime() {
        return userattentioncreatetime;
    }

    public void setUserattentioncreatetime(String userattentioncreatetime) {
        this.userattentioncreatetime = userattentioncreatetime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userattentionid=").append(userattentionid);
        sb.append(", userid=").append(userid);
        sb.append(", fuserid=").append(fuserid);
        sb.append(", userattentiondelstatus=").append(userattentiondelstatus);
        sb.append(", userattentioncreatetime=").append(userattentioncreatetime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}